# WPezRSSCurator
### A WordPress plugin, and an unofficial add-on to the WP RSS Aggregator plugin.
 
 WPezRSSCurator consist of a core plugin (i.e., this repo), as well as a suite of add-ons. Some of the add-ons are free and public, others are premium and can be found here: TODO.
 
 Note: WPezRSSCurator is a separate entity from WP RSS Aggregator.
 
 It's also important to note that WPezRSSCurator is not intended to compete with WP RSS Aggregator and it's ecosytem of free and premium offerings. WPezRSSCurator aims to extend WP RSS Aggregator in ways that allow it to be used in new and previously unimagined ways. Hopefully.
  
 WPezRSSCurator's sweet spot is curation and redistribution via serving RSS feeds from its custom post type (outside of WP RSS Aggregator), as well as the automation there of. Imagine Feedly but with more control and power (and privacy), yet still ez to use. WPezRSSCurator's magic is that its more mindful of your desire to share what you consume. 
 

 ----
 
#### OVERVIEW
 
The better you understand how WPezRSSCurator thinks (and why), the more you'll be able to take advantage of its superpowers. The WPezRSSCurator mindset is a three step process:
 
 **1) RSS Ingestion** -  Here, WP RSS Aggregator is the giant, and upon its shoulders WPezRSSCurator stands.  Curator adds a number of settings to how RSS feeds are ingested (by Aggregator). 
 
  - For example, the +AutoPublish add-on has a setting for what percentage of a feed's incoming items will be auto-published. That is, you can set Curator to auto-publish 50% of a feed's items.  
 
 
 **2) Consumption & Curation** - On the frontend, the addition of various taxonomies to each item (i.e., ingested article) enables you to "filter" your feeds' content in order to find what you want to read and publish. Imagine a WordPress blog as your own private Feedly. 
 
 In addition, taxonomy terms can be defined at the individual feed (in) level, and those terms will automatically be copied, as that feed is ingested, to the individual items as they are added to the WPezRSSCurator customer post type. In short, you can add (taxonomy) terms to your WP RSS Aggregator Feed Sources and those terms are copied to that feed's items as they come in.
 
 - For example, if you know a particular feed's content is mainly about WordPress you can add that terms via one of the taxonomies and then "filter" on the frontend by that taxonomy + term. Think of it as a hyper use of tags.  
 
 - Note: WPezRSSCurator's special theme + add-on is (very much) a work in progress. To date, the majority of the dev time has been on the backend functionality, and specificially automation.
  
 **3) Custom RSS Feeds (Out)** - Ingested items are added to the WPezRSSCuroator custom post type and from there they can published to particular "outbound" feeds - also via a taxonomy + term. 
 
 - With the +Restrict add-on you can also "auth_key protect" your feeds. While not quite a true password, using the auth_key should help to mitigate the risk of "unauthorized" people accessing and reading your outgoing feeds.
    
 - For example, if your Feed (Out) term is test-1, the RSS (out) URL would be: my-site.com/?wpezrss_feed=test-1. Then, any published items assigned Feed (Out) > test-1 would be in that feed.    
  

 **Summary** - There are settings for ingesting (e.g., auto-publish) and settings for the make-up of Feed (Out)s (e.g., use the author @Twitter handle - which is a taxonomy + term - instead of the regular author).
  
  More details are provided below.
   
   ----
  
#### Ways to Use WPezRSSCurator (TODO)
 
  - Personal private Feedly
  - Shared team resource
  - Social Media "preprocessor"
  - RSS to (MailChimp) email blast
  - All of the above
  
  ---
 
#### A brief overview of each add-on: 


  
**WPezRSSCurator +Agency (Free)** - Adds agency-centric taxonomies for frontend filtering.

 ----
 
**WPezRSSCurator +AutoPublish (Premium)** - Aside from the auto-publishing randomization (mentioned above), there are whitelist/blacklist for item authors and/or keywords (in the title and/or content).

----  
**WPezRSSCurator +Email (Premium)** - Coming soon...Share links to articles wth colleagues, staff, friends, etc. 

----
**WPezRSSCurator +GTD (Free)** -  Adds get-things-done-centric taxonomies for frontend filtering.

----
**WPezRSSCurator +Images (Premium)** - Numerous options for ingesting images, as well as adding a featured image to your feeds (out).

----
**WPezRSSCurator +Restrict (Premium)** - Restricts access to all the WordPress feeds for all the Currator taxonomies except the Feed (Out). With this add-on you can also use an (optional) auth_key (i.e., pseudo password) in order to limit access to your feeds.
  
For example, (and building on the example above) if your auth_key was test-2, then the RSS (out) URL would be: my-site.com/?wpezrss_feed=test-1&auth_key=test-2

----
   
 **WPezRSSCurator +Social (Premium)** - Add settings for crafting "social friendly" feeds. If you're going to pair WPezRSSCurator with IFTTT & Buffer then you're going to want to use this add-on. 
 
----
 
 **WPezRSSCurator +Theme (Free)** - Pairs with WPezRSSCurator's TwentySixteen special child theme. Again, very much a work in progress. The immediate focus has been on automation and - for now - curation via the WP Admin side. 
 
----

 **WPezRSSCurator +utm (Premium)** - Add (Google Analytics') utm tags to the feed out's item URLs. This is uber-cool because it can help get you recognied by the sites receiving the traffic (from your social shares). Like +Social, if you're going to pair WPezRSSCurator with IFTTT & Buffer then you're going to want to use this add-on.   
  
--- 

#### Recommendations, Suggestions & Closing Random Thoughts

1 - WPezRSSCurator is intended to be its own WP install (as opposed to adding it to the WP install that runs your website / blog). Ingesting multiple feeds (with images) adds up quickly! In addition, if you're using the +Theme (add-on) along with the special child theme, access to the site's content will be limited to users who are logged in. 

- my-site.com - will show "error"
- my-iste.com/wp-admin/ - will give you the login screen. Just login and make the magic happen. 

2 - There are a lot of options for images. This was driven by two things. First, there's the issue of server load, etc. The work-around is the ability to use images external to the WP install you're running on. Typically, this means having the image served by the source site. (Shhhhh, don't tell anyone.) Two, while testing we bumped into a feed that served images with query string (e.g., ...some-image.png?query=string). This mucked up IFTTT / Buffer. The Bypass setting is a work around for this (rare situation).
  
3 - WordPress likes to cache the RSS feeds it serves. When testing just add (e.g.) &x1 to the query string, and keep changing that. While that bit has no value to WP it will force WP to serve a new version of the requested feed.

4 - For feeds aimed to Twitter, the use Feed (Out) > #Hashtags Placement > Hashtags Only. While anything beyond 140 characters will be dropped, an excess well over 140 seemed to bother IFTTT and/or Buffer.  

5 - For pub'ing to social, use Buffer. Sure, IFTTT will let you go directly to FB, Tw, etc. But until you get comfortable with all the WPezCurator settings Buffer is a nice failsafe to have.

6 - In IFTTT (to Buffer) use: {{EntryTitle}} {{EntryUrl}} {{EntryAuthor}} {{EntryContent}}. The point being, don't put the URL at the end, it might be cutoff depending on the length of the title + author + content (i.e., hashtags). 

7 - Auto-publishing is powerful and fun. How you use it and how much depends on the level of trust you in your RSS sources, and your WPezRSSCurator setting (i.e., whitelist / blacklist).  Naturally, it's also important to consider the "flexibility" (or not) of the brand you're (auto) posting for, as well as the platform you're posting to. That is, it's easy for a lame share to get lost in the noise of Twitter, but LinkedIn might be less forgiving. 