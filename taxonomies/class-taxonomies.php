<?php

/**
 * We're going to make liberal use of taxonomies as a means to filter (read: curate) the
 * various feed / feed items.
 *
 * Some of the feed taxonomies are for setting defaults that are applied to that feed's
 * individual feed items, while others are automatically populated (e.g., feed source, feed source
 * name), and some are for making "manual adjustments" to individual feed items (e.g., TODO).
 *
 * the taxonomy you'll use to get feeds out is feed_rss_out.
 *
 * The ultimate goal is a workflow that is both effective and efficient
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'Taxonomies' ) ) {
	class Taxonomies {


		function __construct() {

			/**
			 * Load up our taxs up boss
			 */
			add_action( 'init', array( $this, 'ez_loader_register_taxonomy' ), 9 );

		}


		/**
		 * https://codex.wordpress.org/Function_Reference/register_taxonomy
		 */
		function ez_loader_register_taxonomy() {

			if ( ! is_array( $this-> wpezrss_taxonomies() ) ) {
				return;
			}
			foreach ( $this-> wpezrss_taxonomies() as $key => $arr_tax ) {
				if ( isset( $arr_tax['active'] ) && $arr_tax['active'] !== false ) {
					if ( isset( $arr_tax['taxonomy'] ) && isset( $arr_tax['object_type'] ) && isset( $arr_tax['args'] ) ) {

						$arr_tax['args'] = $this->register_taxonomy_labels( $arr_tax['args'] );
						// https://codex.wordpress.org/Function_Reference/register_taxonomy
						register_taxonomy(
							$arr_tax['taxonomy'],
							$arr_tax['object_type'],
							$arr_tax['args']
						);
					}
				}
			}
		}

		function register_taxonomy_labels( $arr_args = array() ) {

			if ( isset( $arr_args['labels']['singular_name'] ) ) {

				$str_name_sing = $arr_args['labels']['singular_name'];
				$str_name      = $str_name_sing . 's';
				if ( isset( $arr_args['labels']['name'] ) ) {
					$str_name = $arr_args['labels']['name'];
				}
				
				$arr_taxs = array(
					'name'                       => $str_name,
					'singular_name'              => $str_name_sing,
					'menu_name'                  => '&bull; ' . $str_name,
					'all_items'                  => 'All ' . $str_name,
					'edit_item'                  => 'Edit ' . $str_name_sing,
					'view_item'                  => 'View ' . $str_name_sing,
					'update_item'                => 'Update ' . $str_name_sing,
					'add_new_item'               => 'Add New ' . $str_name_sing,
					'new_item_name'              => 'New ' . $str_name_sing . ' Name',
					'parent_item'                => 'Parent ' . $str_name_sing,
					'parent_item_colon'          => 'Parent ' . $str_name_sing . ':',
					'search_items'               => 'Search ' . $str_name,
					'popular_items'              => 'Popular ' . $str_name,
					'separate_items_with_commas' => 'Separate ' . strtolower( $str_name ) . ' with commas',
					'add_or_remove_items'        => 'Add or remove ' . strtolower( $str_name ),
					'choose_from_most_used'      => 'Choose from the most used ' . strtolower( $str_name ),
					'not_found'                  => 'No ' . strtolower( $str_name ) . ' found',
				);

				$arr_args['labels'] = array_merge( $arr_taxs, $arr_args['labels'] );

				return $arr_args;
			}

			return $arr_args;
		}

		/*
		 * returns an array of taxonomy (names) based on the key requested.
		 * we use this to add our new taxonomies to other post types. maintaining
		 * a single array/list is ez'ier

		function get_array( $str_key = '', $bool_not = false ) {

			$arr_ez_defs = $this-> wpezrss_taxonomies();
			$arr_return  = array();
			if ( is_array( $arr_ez_defs ) ) {

				foreach ( $arr_ez_defs as $key => $arr_tax ) {
					if ( isset( $arr_tax['active'] ) && $arr_tax['active'] != $bool_not ) {
						if ( isset( $arr_tax[ $str_key ] ) && $arr_tax[ $str_key ] != $bool_not && isset( $arr_tax['taxonomy'] ) ) {
							$arr_return[] = $arr_tax['taxonomy'];
						}
					}
				}
			}

			return $arr_return;
		}
		*/

		/*
		 * -------------------------------------
		 * We're going to make liberal use of taxonomies as a means to filter (read: curate) the
		 * various WPezRSSCurate items.
		 *
		 * Some of the feed taxonomies are for setting defaults that are applied to that feed's
		 * individual feed item, some are automatically populated (e.g., feed source, feed source
		 * name), and some are for making "manual adjustments" to individual feed items (e.g., TODO).
		 *
		 * The ultimate goal is a workflow that is both effective and efficient
		 * -------------------------------------
		 */
		function wpezrss_taxonomies( $arr_taxs_override = array() ) {

			/**
			 * NOTE: feed_ is used as a short and sweet universal for feed_ and well as feed_item_
			 */

			$arr_taxs = array();

			/**
			 * Assign this feed_item to which  "outbound" RSS feeds?
			 * When making a feed request this will be our primary tax
			 */
			$arr_taxs['wpezrss_feed'] = array(

				'active'                        => true,
				'taxonomy'    => 'wpezrss_feed',
				'object_type' => array( 'wprss_feed', 'wpezrsscurator' ),
				'args'        => array(
					'label'                 => __( 'WPezRSSCurator Feed Out' ),
					'labels'                => array(
						'name'          => 'Feed (Out)',
						'singular_name' => 'Feed (Out)',
						'menu_name' => '&rarr; Feed (Out)'
					),
					'choose_from_most_used' => __( 'Choose from most used', 'wpezrss' ),
					'rewrite'               => array( 'slug' => 'wpezrss-feed' ),
					'hierarchical'          => true,
					'update_count_callback' => '_update_post_term_count',
					'show_admin_column'     => true,
				)
			);

			/**
			 * $post->post_title from the Feed Source CPT
			 *
			 * Note: In its current (free) form, RSS Agg allows the
			 * feed (post) title to be changed but the post_name is NOT updated
			 */
			$arr_taxs['wpezrss_source'] = array(
				'active'                        => true,
				'taxonomy'    => 'wpezrss_source',
				'object_type' => array( 'wpezrsscurator' ),
				'args'        => array(
					'label'                 => __( 'Feed Source' ),
					'labels'                => array(
						'singular_name' => 'Source',
					),
					'public'                => true,
					'choose_from_most_used' => __( 'Choose from most used', 'wpezrss' ),
					'rewrite'               => array( 'slug' => 'wpezrss-source' ),
					'hierarchical'          => false,
					'update_count_callback' => '_update_post_term_count',
					'show_admin_column'     => true,
				),
			);

			/**
			 * $post->post_name from the RSS Aggregator Feed Source CPT
			 */
			$arr_taxs['wpezrss_source_name'] = array(
				'active'                        => true,
				'taxonomy'    => 'wpezrss_source_name',
				'object_type' => array( 'wpezrsscurator' ),
				'args'        => array(
					'label'                 => __( 'Feed Source Name' ),
					'labels'                => array(
						'singular_name' => 'Source Name',
					),
					'choose_from_most_used' => __( 'Choose from most used', 'wpezrss' ),
					'rewrite'               => array( 'slug' => 'wpezrss-source-name' ),
					'hierarchical'          => false,
					'update_count_callback' => '_update_post_term_count',
					'show_admin_column'     => false,
				),
			);


			/**
			 * The original author(s) of this feed item (*not* the new local WP user we assign
			 * to the feed item post
			 */
			$arr_taxs['wpezrss_author'] = array(
				'active'                        => true,
				'taxonomy'    => 'wpezrss_author',
				'object_type' => array( 'wpezrsscurator' ),
				'args'        => array(
					'label'                 => __( 'Author', 'wpezrss' ),
					'labels'                => array(
						'singular_name' => __( 'Author', 'wpezrss' ),
					),
					'choose_from_most_used' => __( 'Choose from most used', 'wpezrss' ),
					'rewrite'               => array( 'slug' => 'wpezrss-author' ),
					'hierarchical'          => false,
					'update_count_callback' => '_update_post_term_count',
					'show_admin_column'     => false,
				)
			);

			$arr_taxs_social = apply_filters( 'wpezrsscurator_social_taxonomies_add', array() );

			if ( is_array($arr_taxs_social) ){
				// TODO which arr is first? or second?
				$arr_taxs = array_merge($arr_taxs, $arr_taxs_social );
			}


			/**
			 * e.g., Web Design, Web Development, UX
			 *
			 * Kinda like categories without using that standard WP taxonomy
			 *
			 * as a feed is processed these will be automatically added to each new feed_item
			 */
			$arr_taxs['wpezrss_collections'] = array(
				'active'                        => true,
				'taxonomy'    => 'wpezrss_collections',
				'object_type' => array( 'wpezrsscurator', 'wprss_feed' ),
				'args'        => array(
					'label'                 => __( 'Collections', 'wpezrss' ),
					'labels'                => array(
						'singular_name' => __( 'Collection', 'wpezrss' ),
					),
					'choose_from_most_used' => __( 'Choose from most used', 'wpezrss' ),
					'rewrite'               => array( 'slug' => 'wpezrss-collections' ),
					'hierarchical'          => true,
					'update_count_callback' => '_update_post_term_count',
					'show_admin_column'     => true,
				)
			);


			$arr_taxs['wpezrss_tags'] = array(
				'active'                        => true,
				'taxonomy'    => 'wpezrss_tags',
				'object_type' => array( 'wpezrsscurator', 'wprss_feed' ),
				'args'        => array(
					'label'                 => __( 'WPezRSS Tags', 'wpezrss'),
					'labels'                => array(
						'singular_name' =>  __( 'WPezRSS Tag', 'wpezrss'),
					),
					'choose_from_most_used' => __( 'Choose from most used', 'wpezrss' ),
					'rewrite'               => array( 'slug' => 'wpezrss-tags' ),
					'hierarchical'          => false,
					'update_count_callback' => '_update_post_term_count',
					'show_admin_column'     => false,
				)
			);



			/**
			 * e.g. A parent would be (e.g.) BBC or NYTimes with multiple feed children below it.
			 *
			 * as a feed is processed these will be automatically added to each new item
			 */
			$arr_taxs['wpezrss_family'] = array(
				'active'                        => true,
				'taxonomy'    => 'wpezrss_family',
				'object_type' => array( 'wpezrsscurator', 'wprss_feed' ),
				'args'        => array(
					'label'                 => __( 'Family', 'wpezrss' ),
					'labels'                => array(
						'name'          =>  __( 'Families', 'wpezrss' ),
						'singular_name' =>  __( 'Family', 'wpezrss' ),
					),
					'choose_from_most_used' => __( 'Choose from most used', 'wpezrss' ),
					'rewrite'               => array( 'slug' => 'wpezrss-family' ),
					'hierarchical'          => true,
					'update_count_callback' => '_update_post_term_count',
					'show_admin_column'     => false,
				)
			);

		    $arr_taxs_agency = apply_filters( 'wpezrsscurator_agency_taxonomies_add', array() );

			if ( is_array($arr_taxs_agency) ){
				// TODO which arr is first? or second?
				$arr_taxs = array_merge($arr_taxs, $arr_taxs_agency );
			}

			$arr_taxs_gtd = apply_filters( 'wpezrsscurator_gtd_taxonomies_add', array() );

			if ( is_array($arr_taxs_gtd) ){
				// TODO which arr is first? or second?
				$arr_taxs = array_merge($arr_taxs, $arr_taxs_gtd );
			}

			$arr_taxs_override = apply_filters( 'wpezrsscurator_taxonomies_master_override', $arr_taxs );
			//	$this->_arr_defaults = $this->defaults( $arr_taxs_override );

			if ( is_array( $arr_taxs_override ) ) {
				$arr_taxs = array_merge( $arr_taxs, $arr_taxs_override );
			}

			return $arr_taxs;
		}

	}
}