<?php
/*
 * Sanitization as it's own free standing class
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'Sanitize' ) ) {
	class Sanitize {

		protected $_default_method;

		function __construct() {

			$this->_default_method = 'sanitize_text_field';

		}

		public function sanitize($str_method, $value, $arr_args = array()){

			// maybe we want to live dangerously and not sanitize
			if ( $str_method === false) {
				return $value;
			}
			if ( method_exists($this, $str_method)){
				return $this->$str_method($value, $arr_args);
			}
			return $this->$this->_default_method($value, $arr_args);

		}

		// https://codex.wordpress.org/Function_Reference/sanitize_email
		protected function sanitize_email($value, $arr_args = array()) {
			return sanitize_email( $value );
		}

		// https://codex.wordpress.org/Function_Reference/sanitize_file_name
		protected function sanitize_file_name($value, $arr_args = array()) {
			return sanitize_file_name( $value );
		}

		// https://codex.wordpress.org/Function_Reference/sanitize_html_class
		// "Sanitizes a html classname to ensure it only contains valid characters."
		protected function sanitize_html_class($value, $arr_args = array()) {
			return sanitize_html_class( $value );
		}

		// https://codex.wordpress.org/Function_Reference/sanitize_key
		protected function sanitize_key($value, $arr_args = array()) {
				return sanitize_key( $value );
		}

		// https://codex.wordpress.org/Function_Reference/sanitize_meta
		protected function sanitize_meta($value, $arr_args = array()) {
			if ( isset($arr_args['register_meta']['meta_key']) && isset($arr_args['wp_type']) ) {
				return sanitize_meta( $arr_args['register_meta']['meta_key'], $value, $arr_args['wp_type'] );
			}
			return $value;
		}

		// https://codex.wordpress.org/Function_Reference/sanitize_mime_type
		protected function sanitize_mime_type($value, $arr_args = array()) {
			return sanitize_mime_type( $value );
		}

		// https://codex.wordpress.org/Function_Reference/sanitize_option
		protected function sanitize_option($value, $arr_args = array()){
			return sanitize_option( $value );
		}

		// https://codex.wordpress.org/Function_Reference/sanitize_sql_orderby
		protected function sanitize_sql_orderby($value, $arr_args = array()) {
			return sanitize_sql_orderby( $value );
		}

		// https://developer.wordpress.org/reference/functions/sanitize_text_field/
		protected function sanitize_text_field($value, $arr_args = array()) {
			return sanitize_text_field( $value );
		}

		// https://codex.wordpress.org/Function_Reference/sanitize_title
		protected function sanitize_title($value, $arr_args = array()) {
			return sanitize_title( $value );
		}

		// https://codex.wordpress.org/Function_Reference/sanitize_title_for_query
		protected function sanitize_title_for_query($value, $arr_args = array()) {
			return sanitize_title_for_query( $value );
		}

		// https://codex.wordpress.org/Function_Reference/sanitize_title_with_dashes
		protected function sanitize_title_with_dashes($value, $arr_args = array()) {
			return sanitize_title_with_dashes( $value );
		}

		// https://codex.wordpress.org/Function_Reference/sanitize_user
		protected function sanitize_user($value, $arr_args = array()) {
			return sanitize_user( $value );
		}

		// https://codex.wordpress.org/wp_kses_post
		protected function wp_kses_post($value, $arr_args = array()) {
			return wp_kses_post( $value );
		}

		// https://codex.wordpress.org/Function_Reference/tag_escape
		// "Removes all characters other than a-zA-Z0-9_:, the set of valid HTML
		// tag characters. Transforms letters to lowercase."
		protected function esc_tag($value, $arr_args = array()) {
			return tag_escape($value);
		}
		protected function tag_escape($value, $arr_args = array()) {
			return tag_escape($value);
		}


		// https://codex.wordpress.org/Function_Reference/esc_textarea
		// Encodes text for use inside a <textarea> element.
		protected function esc_textarea($value, $arr_args = array()) {
			return esc_textarea($value);
		}

		// https://developer.wordpress.org/reference/functions/esc_url/
		protected function esc_url($value, $arr_args = array()){
			return esc_url($value);
		}


		// https://codex.wordpress.org/Function_Reference/wp_strip_all_tagss
		// Properly strip all HTML tags including script and style.
		protected function  wp_strip_all_tags($value, $arr_args = array()){
			$bool_remove_breaks = false;
			if ( isset ($srr_args['remove_breaks']) ){
				$bool_remove_breaks = (bool)$srr_args['remove_breaks'];
			}
			return  wp_strip_all_tags($value, $bool_remove_breaks);
		}

		// https://codex.wordpress.org/Function_Reference/wptexturize
		//
		// "This returns given text with transformations of quotes to smart
		// quotes, apostrophes, dashes, ellipses, the trademark symbol, and
		// the multiplication symbol."
		protected function wptexturize($value, $arr_args = array()){
			return wptexturize($value);
		}

		// http://themehybrid.com/weblog/introduction-to-wordpress-term-meta
		protected function sanitize_hex($value, $arr_args = array()) {
			$value = ltrim( $value, '#' );
			return preg_match( '/([A-Fa-f0-9]{3}){1,2}$/', $value ) ? $value : '';
		}


		// checkbool is a special ez form element for yes (checked) or no (unchecked)
		protected function sanitize_checkbool($value, $arr_args = array()) {
			if ( ! empty($value) ){
				return 'on';
			}
			return '';
		}


	}
}