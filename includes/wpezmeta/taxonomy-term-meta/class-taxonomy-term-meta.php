<?php
/**
 * Got a taxonomy?
 * Wanna add some metas? This is The ezWay to do it. Seriously, it's THIS ez :)
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'Taxonomy_Term_Meta' ) ) {
	class Taxonomy_Term_Meta extends Form_Elements {

		protected $_priorities;
		protected $_taxonomy;
		protected $_register_meta_object_types;
		protected $_prefix;
		protected $_wp_nonce;
		protected $_wpezfields;

		function __construct( $arr_args = array() ) {

			// just in case you want more control - see below for defaults
			$this->_priorities = $this->priorities_defaults();
			if ( isset( $arr_arg['priorities'] ) && is_array( $arr_arg['priorities'] ) ) {
				$this->_priorities = array_merge( $this->priorities_defaults(), $arr_arg['priorities']);
			}

			if ( $this->_priorities['admin_enqueue_scripts'] !== false ) {
				add_action( 'admin_enqueue_scripts', array( $this, 'wp_enqueue_scripts_action' ), $this->_priorities['admin_enqueue_scripts'] );
			}

			$this->_register_meta_object_types = array();
			// register meta - as mentioned here: http://themehybrid.com/weblog/introduction-to-wordpress-term-meta
			if ( $this->_priorities['register_meta'] !== false ) {
				add_action( 'init', array($this, 'register_meta'), $this->_priorities['register_meta'] );
			}

		}

		protected function priorities_defaults() {

			$arr_defs = array(
				'admin_enqueue_scripts' => 20,
				'register_meta'    => 20,
				'add_form_fields'  => 10,
				'edit_form_fields' => 10,
				'create'           => 10,
				'edit'             => 10
			);

			return $arr_defs;
		}


		public function ez_loader($arr_args = array() ){

			if ( isset( $arr_args['active'] ) && $arr_args['active'] === true ) {

				// more or less required
				if ( isset( $arr_args['taxonomy_name'] ) ) {
					$this->_taxonomy = $arr_args['taxonomy_name'];
				}
				$this->_register_meta_object_types[] = $this->_taxonomy;

				// add form
				if ( $this->_priorities['add_form_fields'] !== false ) {
					add_action( $this->_taxonomy . '_add_form_fields', array($this, 'add_form_fields_action' ), $this->_priorities['add_form_fields'] );
				}
				// edit form
				if ( $this->_priorities['edit_form_fields'] !== false ) {
					add_action( $this->_taxonomy . '_edit_form_fields', array($this, 'edit_form_fields_action'), $this->_priorities['edit_form_fields'] );
				}
				// create + edit = aka save
				if ( $this->_priorities['create'] !== false ) {
					add_action( 'create_' . $this->_taxonomy, array( $this, 'create_edit' ), $this->_priorities['create'] );
				}
				if ( $this->_priorities['edit'] !== false ) {
					add_action( 'edit_' . $this->_taxonomy, array( $this, 'create_edit' ), $this->_priorities['edit'] );
				}

				if ( isset( $arr_args['prefix'] ) ) {
					$this->set_prefix($arr_args['prefix']);
				}

				// array - wpezfields ftw!!
				if ( isset( $arr_args['wpezfields'] ) && is_array( $arr_args['wpezfields'] ) ) {
					$this->set_wpezfields($arr_args['wpezfields']);
				}

				// array - keys: 'action', 'name', 'referer', 'echo'
				if ( isset( $arr_args['wp_nonce'] ) && is_array( $arr_args['wp_nonce'] ) ) {
					//$this->_wp_nonce = array_merge($this->_wp_nonce, $arr_args['wp_nonce']);
					$this->_wp_nonce = $arr_args['wp_nonce'];
				}
			}
		}


		function register_meta() {
			if ( is_array($this->_wpezfields)) {
				$this->ez_register_meta( $this->_wpezfields, $this->_prefix );
			}
		}


		function add_form_fields_action() {

			$this->ez_nonce( $this->_wp_nonce );
			echo $this->ez_loader_add_form_fields( $this->_wpezfields, $this->_prefix );

		}

		/**
		 * Add a taxonomy term field(s)
		 *
		 * @param $arr_args
		 */
		public function ez_loader_add_form_fields( $arr_wpezfields = array(), $str_prefix = '' ){

			$this->_prefix      = $str_prefix;
			$this->_crud    = 'add_form_fields';
			$this->_wrapper = 'add_form_fields';

			return $this->ez_render( $arr_wpezfields );
		}

		function edit_form_fields_action( $term ) {

			$this->ez_nonce( $this->_wp_nonce );
			echo $this->ez_loader_edit_form_fields( $this->_wpezfields, $this->_prefix, $term );

		}

		/**
		 * Edit a taxonomy term field(s)
		 *
		 * @param $arr_args
		 * @param $term
		 */
		public function ez_loader_edit_form_fields( $arr_wpezfields = array(), $str_prefix = '', $term ){

			$this->_prefix      = $str_prefix;
			$this->_crud    = 'edit_form_fields';
			$this->_wrapper = 'edit_form_fields';
			$this->_term    = $term;

			return $this->ez_render( $arr_wpezfields);
		}

		/**
		 * aka save
		 *
		 * @param $term_id
		 */
		function create_edit( $term_id ) {

			$this->ez_nonce( $this->_wp_nonce );
			$this->ez_loader_save_form_fields( $this->_wpezfields, $this->_prefix, $term_id );

		}

		public function ez_loader_save_form_fields( $arr_wpezfields = array(), $str_prefix = '', $term_id ) {

			$this->_prefix      = $str_prefix;
			// if we have a nonce then we need to check it
			if ( $this->_nonce_active === true ) {

				if ( ! isset( $_POST[ $this->_nonce_name ] ) || ! wp_verify_nonce( $_POST[ $this->_nonce_name ], $this->_nonce_action ) ) {
					return;
				}
			}

			$this->ez_loader_save_fields( $arr_wpezfields, $term_id, 'get_term_meta', 'add_term_meta', 'update_term_meta', 'delete_term_meta' );
		}

	}
}