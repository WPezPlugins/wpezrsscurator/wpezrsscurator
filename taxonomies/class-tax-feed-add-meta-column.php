<?php
/**
 * TODO
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'Tax_Feed_Add_Meta_Column' ) ) {
	class Tax_Feed_Add_Meta_Column {

		protected $_column_meta_key;
		protected $_column_label;

		function __construct() {

			$str_tax = 'wpezrss_feed';

			// TODO - make this filterable
			$this->_column_meta_key = 'wpezrsscurator_wpezrss_feed';
			$this->_column_label = 'WPezRSSCurator';

			// http://themehybrid.com/weblog/introduction-to-wordpress-term-meta
			add_filter( 'manage_edit-' . $str_tax . '_columns', array($this, 'manage_columns_filter') );
			add_filter( 'manage_' . $str_tax . '_custom_column', array($this, 'manage_custom_column_filter'), 20, 3 );

		}


		function manage_columns_filter( $wp_columns ) {

			$wp_columns[$this->_column_meta_key] = esc_attr($this->_column_label);
			return $wp_columns;
		}

		function manage_custom_column_filter( $out, $column, $term_id ) {

			if ( $this->_column_meta_key === $column ) {

				$auth_key = $this->get_term_meta_auth_key( $term_id, true );

				if ( ! $auth_key ) {
					$auth_key = '';
				}
				$out = esc_attr( $auth_key ) ;
				$a = apply_filters('manage_feed_rss_out_custom_column2', $out, $column, $term_id);
				return  $a . '<b>&bull; ' . 'TODO' . '</b><br> - ' . $out;

			}

			return $out;
		}


		function get_term_meta_auth_key( $term_id, $hash = false ) {
			return get_term_meta( $term_id, $this->_column_meta_key, true );
		}


	}
}