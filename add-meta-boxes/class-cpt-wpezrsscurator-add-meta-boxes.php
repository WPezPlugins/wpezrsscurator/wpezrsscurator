<?php
/**
 * The core's meta box for the wpezrsscurator happens here
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'CPT_WPezRSSCurator_Add_Meta_Boxes' ) ) {
	class CPT_WPezRSSCurator_Add_Meta_Boxes {

		protected $_post_types;
	//	protected $_nonce_name;

		function __construct() {

			$this->_post_types = array( 'wpezrsscurator');
		//	$this->_nonce_name = 'wpezrsscurator_add_meta_boxes_nonce';

			add_action( 'init', array( $this, 'init_wpezmeta_add_meta_boxes' ), 10 );
		}


		public function init_wpezmeta_add_meta_boxes() {

			$arr_args              = array(
				'active'       => true,
				'post_types'   => $this->_post_types,
				'add_meta_box' => $this->add_meta_box(),
				'prefix'       => '',
				'wpezfields'    => $this->wpezfields(),  // we pass an empty array because we'll set it later / below
				// optional
				/*'wp_nonce'     => array(
					'action' => basename( __FILE__ ),
					'name'   => $this->_nonce_name,
				)
				*/
			);
			$arr_priorities        = array();
			$new_Add_Meta_Boxe = new Add_Meta_Boxes( $arr_priorities );

			$new_Sanitize = new Sanitize();
			$new_Add_Meta_Boxe->set_sanitize($new_Sanitize);
			$new_Add_Meta_Boxe->ez_loader( $arr_args );
		}


		/**
		 * the args for the add_meta_box
		 *
		 * @return array
		 */
		protected function add_meta_box() {

			$arr_amb = array(
				'id'    => 'cpt-wpezrsscurator-meta-box-id',
				'title' => __( 'WPezRSSCurator', 'wpezrss' ),
				// Optional
				//'callback' => 'add_meta_box_callback',
				// Optional - if not specified, post_types passed via loader will be used
				// 'screen' => $this->_post_type,
				// Optional
				//'context' => 'advanced',
				// Optional
				// 'priority' => 'high',
				//	'callback_args'
			);

			return $arr_amb;
		}


		public function wpezfields() {

			$arr_wpezfields = array();

			$arr_wpezfields['wpezrss_item_permalink'] = array(
				'active'        => true,
				'name'          => 'wpezrss_item_permalink',
				'label'         => __('Article URL', 'wpezrss'),
				'desc'          => __('From the original item.','wpezrss'),
				'type'          => 'text',
				//'default_value' => '',
				// As seen here: https://codex.wordpress.org/Function_Reference/sanitize_meta
				'wp_type'       => 'post',

				// https://codex.wordpress.org/Function_Reference/register_meta
				'register_meta' => array(
					'active'      => true,
					'meta_key'    => 'wpezrss_item_permalink',
					'args'        => array(
						// 'sanitize_callback' => 'sanitize_my_column_meta_key',
						// 'auth_callback' => 'authorize_my_column_meta_key',
						'type'         => 'string',
						'description'  => 'The URL of the Original Article',
						'single'       => true,
						'show_in_rest' => false,
					)
				),
				// the rest are for our field gen'er

				//'wrapper_class' => 'wpezrss-item-permalink-wrap',
				//'id'            => 'wpezrss-item-permalink',
				//'class'         => 'wpezrss-item-permalink',
				//'sanitize'      => 'sanitize_text_field',
				//'validation'    => array(), // TODO
			);

			$arr_wpezfields['wpezrss_item_publish_date'] = array(
				'active'        => true,
				// the rest are for our field gen'er
				'name'          => 'wpezrss_item_publish_date',
				'label'         => __('Publish Date', 'wpezrss'),
				'desc'          => __('From the original item.', 'wpezrss'),
				'type'          => 'text',
				'default_value' => '',
				'wp_type'       => 'post',
				// https://codex.wordpress.org/Function_Reference/register_meta
				'register_meta' => array(
					'active'      => true,
					'meta_key'    => 'wpezrss_item_publish_date',
					'args'        => array(
						// 'sanitize_callback' => 'sanitize_my_column_meta_key',
						// 'auth_callback' => 'authorize_my_column_meta_key',
						'type'         => 'string',
						'description'  => 'Publish Date of the Original Article',
						'single'       => true,
						'show_in_rest' => false,
					)
				),
			);

			$arr_wpezfields['wpezrss_item_publish_date_gmt'] = array(
				'active'        => true,
				// the rest are for our field gen'er
				'name'          => 'wpezrss_item_publish_date_gmt',
				'label'         => __('Publish Date GMT', 'wpezrss'),
				'desc'          => __('From the original item.', 'wpezrss'),
				'type'          => 'text',
				'default_value' => '',
				'wp_type'       => 'post',
				// https://codex.wordpress.org/Function_Reference/register_meta
				'register_meta' => array(
					'active'      => true,
					'meta_key'    => 'wpezrss_item_publish_date_gmt',
					'args'        => array(
						// 'sanitize_callback' => 'sanitize_my_column_meta_key',
						// 'auth_callback' => 'authorize_my_column_meta_key',
						'type'         => 'string',
						'description'  => 'Publish Date GMT of the Original Article',
						'single'       => true,
						'show_in_rest' => false,
					)
				),
			);


			$arr_wpezfields['wpezrss_item_author'] = array(
				'active'        => true,
				// the rest are for our field gen'er
				'name'          => 'wpezrss_item_author',
				'label'         => __('Article Author', 'wpezrss'),
				'desc'          => __('From the original imte.', 'wpezrss'),
				'type'          => 'text',
				// 'default_value' => '',
				'wp_type'       => 'post',
				// https://codex.wordpress.org/Function_Reference/register_meta
				'register_meta' => array(
					'active'      => true,
					'meta_key'    => 'wpezrss_item_author',
					'args'        => array(
						// 'sanitize_callback' => 'sanitize_my_column_meta_key',
						// 'auth_callback' => 'authorize_my_column_meta_key',
						'type'         => 'string',
						'description'  => 'Author of the Original Article',
						'single'       => true,
						'show_in_rest' => false,
					)
				),
			);

			$arr_wpezfields['wpezrss_item_enclosure'] = array(
				'active'        => true,
				'name'          => 'wpezrss_item_enclosure',
				'label'         => __('WP RSS Agg Enclosure URL', 'wpezrss'),
				'desc'          => __('From the original item.','wpezrss'),
				'type'          => 'text',
				//'default_value' => '',
				// As seen here: https://codex.wordpress.org/Function_Reference/sanitize_meta
				'wp_type'       => 'post',

				// https://codex.wordpress.org/Function_Reference/register_meta
				'register_meta' => array(
					'active'      => true,
					'meta_key'    => 'wpezrss_item_enclosure',
					'args'        => array(
						// 'sanitize_callback' => 'sanitize_my_column_meta_key',
						// 'auth_callback' => 'authorize_my_column_meta_key',
						'type'         => 'string',
						'description'  => 'Enclosure URL from the Original Article',
						'single'       => true,
						'show_in_rest' => false,
					)
				),
				// the rest are for our field gen'er

				//'wrapper_class' => 'wpezrss-item-permalink-wrap',
				//'id'            => 'wpezrss-item-permalink',
				//'class'         => 'wpezrss-item-permalink',
				//'sanitize'      => 'sanitize_text_field',
				//'validation'    => array(), // TODO
			);

			$arr_wpezfields['wpezrss_wprssagg_feed_id'] = array(
				'active' => true,

				'name'  => 'wpezrss_wprssagg_feed_id',
				'label' => __('WP RSS Aggregator feed_ID', 'wpezrss'),
				'desc'  => __('From the original feed of this item.', 'wpezrss'),

				'type'          => 'text',
				// 'default_value' => '',
				'wp_type'       => 'post',
				// https://codex.wordpress.org/Function_Reference/register_meta
				'register_meta' => array(
					'active'      => true,
					'meta_key'    => 'wpezrss_wprssagg_feed_id',
					'args'        => array(
						// 'sanitize_callback' => 'sanitize_my_column_meta_key',
						// 'auth_callback' => 'authorize_my_column_meta_key',
						'type'         => 'string',
						'description'  => 'WP RSS Aggregator feed_ID',
						'single'       => true,
						'show_in_rest' => false,
					)
				),
			);

			$arr_wpezfields['wpezrss_item_language'] = array(
				'active' => true,

				'name'  => 'wpezrss_item_language',
				'label' => __('Language', 'wpezrss'),
				'desc'  => __('From the original feed.', 'wpezrss'),
				'type'  => 'text',

				'default_value' => '',
				'wp_type'       => 'post',
				// https://codex.wordpress.org/Function_Reference/register_meta
				'register_meta' => array(
					'active'      => true,
					'meta_key'    => 'wpezrss_item_language',
					'args'        => array(
						// 'sanitize_callback' => 'sanitize_my_column_meta_key',
						// 'auth_callback' => 'authorize_my_column_meta_key',
						'type'         => 'string',
						'description'  => 'Language of the original feed',
						'single'       => true,
						'show_in_rest' => false,
					)
				),
			);

			return $arr_wpezfields;
		}
	}
}