<?php
/**
 * WP RSS Agg provides some hooks (mostly filters), and this is where we take advantage of them
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'WPRss_Hooks' ) ) {
	class WPRss_Hooks {

		protected $_obj_feed;
		protected $_arr_defaults;
		protected $_arr_taxonomies = array();

		public function __construct( $args = '' ) {

			$this->_arr_defaults   = $this->defaults();

			add_action('init', array($this, 'set_taxonomies'), 100);

			//let's add the feed_id to the feed_item (for later use)
			add_filter( 'wprss_insert_post_item_conditionals', array($this,'wprss_insert_post_item_conditionals_filter'), 20, 3 );

			//do_action('wprss_fetch_feed_after', $feed);
			add_action( 'wprss_fetch_feed_after', array( $this, 'wprss_fetch_feed_after_action' ) );

			/**
			 * do_action( 'wprss_items_create_post_meta', $inserted_ID, $item, $feed_ID );
			 *
			 * -- found in feed-importing line 602
			 */
			add_action( 'wprss_items_create_post_meta', array( $this, 'wprss_items_create_post_meta_action' ), 20, 3 );
		}

		public function wprss_fetch_feed_after_action( $feed ) {
			$this->_obj_feed = $feed;
		}

		public function set_taxonomies() {
			$this->_arr_taxonomies = get_object_taxonomies('wprss_feed', 'names');
		}

		public function defaults() {

			$arr_defs = array(
				// 'post_status'       => 'draft',
				'post_type'         => 'wpezrsscurator'
			);

			$arr_def_override = apply_filters( 'wpezrss_wprss_hooks_defaults', $arr_defs );

			if ( is_array( $arr_def_override ) ) {
				return array_merge( $arr_defs, $arr_def_override );
			}
			return $arr_defs;
		}


		/**
		 *    let's add the feed_id to the feed_item (for later use)
		 *
		 * @param $item
		 * @param $feed_ID
		 * @param $permalink
		 *
		 * @return mixed
		 */
		function wprss_insert_post_item_conditionals_filter( $item, $feed_ID, $permalink ) {

			$item->feed_id = $feed_ID;
			return $item;
		}


		/**
		 * we're gonna let WP RSS Agg do it's thing. Those feed_items will serves as an "audit trail" or sorts.
		 * The WPezRSSCurator magic will exist outside that.
		 *
		 * @param string $wprss_inserted_ID
		 * @param string $wprss_item
		 * @param string $wprss_feed_ID
		 *
		 * @return bool|int|\WP_Error
		 */
		function insert_post( $wprss_inserted_ID = '', $wprss_item = '', $wprss_feed_ID = '' ) {

			// If the item is not NULL, continue to inserting the feed item post into the DB
			if ( $wprss_item !== NULL && ! is_bool( $wprss_item ) ) {
				wprss_log( 'WPezRSS: Using core logic', NULL, WPRSS_LOG_LEVEL_SYSTEM );

				$arr_defs = $this->_arr_defaults;

				$post_status = 'draft';
				$str_post_status_publish = get_post_meta( $wprss_item->feed_id, 'wpezrss_auto_publish', true );
				if ( $str_post_status_publish == '10' ) {
					$post_status = 'publish';
				} elseif ( intval($str_post_status_publish) > 0 &&  intval($str_post_status_publish) < 10 ){
					$max = 10000;
					$rand_int = rand(1, $max);
					if ( $rand_int <= ($max * ( $str_post_status_publish/ 10 )) ){
						$post_status = 'publish';
					}
				}

				$post_status = apply_filters('wpezrss_wprss_insert_post_check_xlist', $post_status, $wprss_feed_ID, $wprss_item );


				$post_type = 'wpezrsscurator';
				if ( isset( $arr_defs['post_type'] ) ) {
					$post_type = sanitize_text_field( $arr_defs['post_type'] );
				}

				// Get the date and GTM date and normalize if not valid dor not present
				$format    = 'Y-m-d H:i:s';
			//	$has_date  = $wprss_item->get_date( 'U' ) ? true : false;
			//	$timestamp = $has_date ? $wprss_item->get_date( 'U' ) : date( 'U' );
				$timestamp = date( 'U' );
				$date      = date( $format, $timestamp );
				$date_gmt  = gmdate( $format, $timestamp );
				// Prepare the item data
				$wpezrss_feed_item = apply_filters(
					'wpezrss_populate_post_data',
					array(
						'post_title'    => html_entity_decode( $wprss_item->get_title() ),
						'post_content'  => $wprss_item->get_content(),
						'post_excerpt'  => $wprss_item->get_description(),
						'post_status'   => $post_status,
						'post_type'     => $post_type,
						//'post_author' => TODO
						'post_date'     => $date,
						'post_date_gmt' => $date_gmt
					),
					$wprss_item
				);
				wprss_log( 'WPezRSS: Post data filters applied', NULL, WPRSS_LOG_LEVEL_SYSTEM );

				/*
				if ( defined('ICL_SITEPRESS_VERSION') )
					@include_once( WP_PLUGIN_DIR . '/sitepress-multilingual-cms/inc/wpml-api.php' );
				if ( defined('ICL_LANGUAGE_CODE') ) {
					$_POST['icl_post_language'] = $language_code = ICL_LANGUAGE_CODE;
					wprss_log_obj( 'WPML detected. Language code determined', $language_code, null, WPRSS_LOG_LEVEL_SYSTEM );
				}
				*/

				// Create and insert post object into the DB
				$wpezrss_inserted_ID = wp_insert_post( $wpezrss_feed_item );

				if ( ! is_wp_error( $wpezrss_inserted_ID ) ) {

					if ( is_object( $wpezrss_inserted_ID ) ) {
						if ( isset( $wpezrss_inserted_ID['ID'] ) ) {
							$wpezrss_inserted_ID = $wpezrss_inserted_ID['ID'];
						} elseif ( isset( $wpezrss_inserted_ID->ID ) ) {
							$wpezrss_inserted_ID = $wpezrss_inserted_ID->ID;
						}
					}

					// get the wprss metas
					$wprss_permalink     = get_post_meta( $wprss_inserted_ID, 'wprss_item_permalink', true );
					$wprss_enclosure_url = get_post_meta( $wprss_inserted_ID, 'wprss_item_enclosure', true );

					// Increment the inserted items counter
					// $items_inserted++;

					// this hand this off to the images add-on
					// $str_item_content = $wprss_item->get_content();
					//do_action('wpezrss_curator_insert_post_meta_images', $wpezrss_inserted_ID, $str_item_content);

					// now take the wprss meta and "re-insert" it for our new (cpt) post
					$this->insert_post_meta( $wpezrss_inserted_ID, $wprss_item, $wprss_feed_ID, $wprss_permalink, $wprss_enclosure_url );

					do_action('wpezrsscurator_insert_post_meta_complete', $this->_obj_feed, $wpezrss_inserted_ID, $post_status, $wprss_item, $wprss_feed_ID);

					// Remember newly added permalink
					// $existing_permalinks[$wprss_permalink] = 1;
					wprss_log_obj( 'WPezRSS: Item imported', $wpezrss_inserted_ID, NULL, WPRSS_LOG_LEVEL_INFO );

					return $wpezrss_inserted_ID;
				} else {
					// update_post_meta( $source, 'wprss_error_last_import', 'An error occurred while inserting a feed item into the database.' );
					wprss_log_obj( 'WPezRSS: Failed to insert post', $wpezrss_feed_item, 'wprss_items_insert_post > wp_insert_post' );
				}
			}

			return false;
		}

		/**
		 * Inserts the appropriate post meta for feed items.
		 *
		 * Called from 'wprss_items_insert_post'
		 *
		 * @since 2.3
		 */
		function insert_post_meta( $inserted_ID = '', $item = '', $feed_ID = '', $permalink = '', $enclosure_url = '' ) {

			// note: wpezrss_item_permalink is also used in class-is-feed
			update_post_meta( $inserted_ID, 'wpezrss_item_permalink', $permalink );
			update_post_meta( $inserted_ID, 'wpezrss_item_enclosure', $enclosure_url );

			// TODO - add other meta from the original feed item?

			// item pub date
			$str_date = $item->get_date( 'U' );
			if ( $str_date ) {
				$format = 'Y-m-d H:i:s';
				update_post_meta( $inserted_ID, 'wpezrss_item_publish_date', date( $format, $str_date ) );
			}

			// item pub date gmt
			$str_date_gmt = $item->get_date( 'U' );
			if ( $str_date_gmt ) {
				$format = 'Y-m-d H:i:s';
				update_post_meta( $inserted_ID, 'wpezrss_item_publish_date_gmt', gmdate( $format, $str_date_gmt ) );
			}

			// this is for the feed - not the item.
			$str_type = $this->_obj_feed->get_type();
			if ( $str_type ) {
				update_post_meta( $feed_ID, 'wpezrss_feed_type', $str_type );
			}

			// this is a feed level prop but we'll store at item level
			$str_lang = $this->_obj_feed->get_language();
			if ( $str_lang ) {
				update_post_meta( $inserted_ID, 'wpezrss_item_language', $str_lang );
			}

			// this is a feed level prop but we'll store at item level
			$str_img_url = $this->_obj_feed->get_image_url();
			if ( $str_img_url ) {
				update_post_meta( $inserted_ID, 'wpezrss_item_image_url', $str_img_url );
			}


			$author = $item->get_author();
			if ( $author ) {
				// TODO - safe to assume there's ->get_name()?
				update_post_meta( $inserted_ID, 'wpezrss_item_author', $author->get_name() );
			}

			update_post_meta( $inserted_ID, 'wpezrss_wprssagg_feed_id', $feed_ID );

			if ( 1 == 2 ) {


				$gpm_feed = get_post_meta( $item->feed_id );

				$str_auto_feat_img = false;
				if ( isset( $gpm_feed['wprss_wpezrss_post_featured_image'][0] ) ) {
					$str_auto_feat_img = $gpm_feed['wprss_wpezrss_post_featured_image'][0];
				}

				//$str_auto_feat_img = get_post_meta( $item->feed_id, 'wprss_wpezrss_post_featured_image', true );
				// TODO look for the X - to be int
				if ( $str_auto_feat_img != 'no' && ! empty( $str_auto_feat_img ) && $str_auto_feat_img != false ) {
					do_action( 'wpezrss_post_featured_image', $inserted_ID, $item, $feed_ID, $str_auto_feat_img );
				}
			}

		}

		function wprss_items_create_post_meta_action( $wprss_inserted_ID = '', $wprss_item = '', $wprss_feed_ID = '' ) {

			// now we want to add our own
			$wpezrss_inserted_ID = $this->insert_post( $wprss_inserted_ID, $wprss_item, $wprss_feed_ID );

			if ( $wpezrss_inserted_ID !== false ) {
				// the value of these taxs are "calculated" and created from scratch
				$ret_bool = $this->wp_set_post_terms_feed_item_create( $wpezrss_inserted_ID, $wprss_item, $wprss_feed_ID );

				// TODO - some action?

				// copy the feed defaults over to the "feed item(s)" as those new posts are add in WP
				$ret_bool = $this->wp_set_post_terms_feed_item_copy( $wpezrss_inserted_ID, $wprss_item, $wprss_feed_ID );

			}
		}

		/**
		 * we create these from the original post's values
		 */
		function wp_set_post_terms_feed_item_create( $inserted_ID = '', $item = '', $feed_ID = '' ) {

			if ( ! empty( $feed_ID ) ) {

				$arr_rt = $this->_arr_taxonomies;

				$int_wprss_feed_id      = (int) $feed_ID;
				$obj_wprss_feed_id_post = get_post( $int_wprss_feed_id );

				$tax = 'wpezrss_source';
				if ( taxonomy_exists( $tax ) ) {
					$feed_source = $obj_wprss_feed_id_post->post_title;
					if ( ! empty( $feed_source ) ) {
						$append = true;
						wp_set_post_terms( $inserted_ID, $feed_source, $tax, $append );
					}
				}

				$tax = 'wpezrss_source_name';
				if ( taxonomy_exists( $tax ) ) {
					$feed_source_name = $obj_wprss_feed_id_post->post_name;
					if ( ! empty( $feed_source_name ) ) {
						$append = true;
						wp_set_post_terms( $inserted_ID, $feed_source_name, $tax, $append );
					}
				}

				$tax = 'wpezrss_author';
				if ( taxonomy_exists( $tax ) ) {
					if ( ! empty( $item->get_author() ) ) {
						$append = true;
						wp_set_post_terms( $inserted_ID, $item->get_author()->get_name(), $tax, $append );
					}
				}

				// TODO - some action?
				return true;
			}

			return false;
		}


		/**
		 * copy the feed defaults over to the feed item(s) as they are created
		 */
		function wp_set_post_terms_feed_item_copy( $inserted_ID = '', $item = '', $feed_ID = '' ) {

			if ( ! empty( $feed_ID ) ) {

				$int_feed_id = (int) $feed_ID;
				$arr_taxs = $this->_arr_taxonomies;

				// if there's a feed cpt tax then copy it forward to the
				foreach ( $arr_taxs as $tax ) {

					// we're only interested in the wpezrss_ taxes
					if ( substr( $tax, 0, 7 ) != 'wpezrss' ) {
						continue;
					}
					// https://developer.wordpress.org/reference/functions/get_the_terms/
					$get_the_terms = get_the_terms( $int_feed_id, $tax );
					// TODO if empty
					$arr_feed_term_ids = $this->get_the_terms_array_ids( $get_the_terms );
					wp_set_post_terms( $inserted_ID, $arr_feed_term_ids, $tax, false );
				}
				// TODO - some action?
				return true;
			}

			return false;
		}

		function get_the_terms_array_ids( $get_the_terms ) {

			$arr_feed_term_ids = array();
			if ( is_array( $get_the_terms ) ) {
				foreach ( $get_the_terms as $feed_term ) {
					$arr_feed_term_ids[] = $feed_term->term_id;
				}
			}
			return $arr_feed_term_ids;
		}

	}
}