<?php
/**
 * Of course you can hand-craft your own post meta box (and still used WPezFormElements) but this ez-tizes matters even further.
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}


if ( ! class_exists( 'Add_Meta_Boxes' ) ) {
	class Add_Meta_Boxes extends Form_Elements{

		protected $_post_types;
		protected $_register_meta_object_types;
		protected $_meta_box_id;

		protected $_add_meta_box;
		protected $_prefix;
		protected $_wpezfields;
		protected $_wp_nonce;

		function __construct( $arr_args = array() ) {

			// just in case you want more control - see below for defaults
			$arr_priorities = $this->priorities_defaults();
			if ( isset( $arr_arg['priorities'] ) && is_array( $arr_arg['priorities'] ) ) {
				$arr_priorities = array_merge( $this->priorities_defaults(), $arr_arg['priorities']);
			}

			if ( $arr_priorities['admin_enqueue_scripts'] !== false ) {
				add_action( 'admin_enqueue_scripts', array( $this, 'wp_enqueue_scripts_action' ), $arr_priorities['admin_enqueue_scripts'] );
			}

			$this->_register_meta_object_types = array();
			// register meta (as seen mentioned here: http://themehybrid.com/weblog/introduction-to-wordpress-term-meta )
			if ( $arr_priorities['register_meta'] !== false ) {
				add_action( 'init', array( $this, 'register_meta' ), $arr_priorities['register_meta'] );
			}

			// https://rudrastyh.com/wordpress/meta-boxes.html#meta-box-class
			if ( $arr_priorities['add_meta_boxes'] !== false ) {
				add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes_action' ), $arr_priorities['add_meta_boxes'] );
			}
			if ( $arr_priorities['save_post'] !== false ) {
				add_action( 'save_post', array( $this, 'save_post_action' ), $arr_priorities['save_post'], 3 );
			}

		}

		/**
		 * @return array
		 */
		protected function priorities_defaults() {

			$arr_defs = array(
				'admin_enqueue_scripts' => 20,
				'register_meta'    => 11,
				'add_meta_boxes'  => 10,
				'save_post' => 10,
			);
			return $arr_defs;
		}

		public function ez_loader($arr_args = array() ){

			if ( isset( $arr_args['active'] ) && $arr_args['active'] === true ) {

				// more or less required
				if ( isset( $arr_args['post_types'] ) ) {
					$this->_post_types = $arr_args['post_types'];
				}
				$this->_register_meta_object_types = $this->_post_types;

				// array - wpezfields ftw!!
				$this->_add_meta_box = $this->add_meta_box_defaults();
				if ( isset( $arr_args['add_meta_box'] ) && is_array( $arr_args['add_meta_box'] ) ) {
					$this->_add_meta_box = array_merge( $this->add_meta_box_defaults(), $arr_args['add_meta_box'] );
				}

				//
				$this->_meta_box_id = $this->_add_meta_box['id'];

				if ( $this->_add_meta_box['screen'] === false ){
					$this->_add_meta_box['screen'] = $this->_post_types;
				}

				if ( isset( $arr_args['prefix'] ) ) {
					$this->set_prefix($arr_args['prefix']);
				}

				// array - wpezfields ftw!!
				if ( isset( $arr_args['wpezfields'] ) && is_array( $arr_args['wpezfields'] ) ) {
					$this->set_wpezfields($arr_args['wpezfields']);
				}

				$this->_wp_nonce = array(
					'name' => 'nonce_' . sanitize_file_name($this->_add_meta_box['id']) . '_name',
					'action' => 'nonce_' . sanitize_file_name($this->_add_meta_box['id']) . '_action'
				);

				// array - keys: 'action', 'name', 'referer', 'echo'
				if ( isset( $arr_args['wp_nonce'] ) && is_array( $arr_args['wp_nonce'] ) ) {
					$this->_wp_nonce = array_merge($this->_wp_nonce, $arr_args['wp_nonce']);
				}
			}
		}


		/**
		 * @return array
		 */
		protected function add_meta_box_defaults(){

			$arr_amb = array(
				'id' => 'wpezpost-meta-box-id',
				'title' => 'WPezPost_Meta_Box Title',
				'callback' => 'add_meta_box_callback',
				'screen' => false,
				'context' => 'advanced',
				'priority' => 'default',
				'callback_args' => array(),
			);
			return $arr_amb;
		}

		function register_meta() {

			if ( is_array($this->_wpezfields)) {
				$this->ez_register_meta( $this->_wpezfields, $this->_prefix );
			}
		}

		// TODO - allow "manual" builds
		public function add_meta_boxes_action( $arr_add_meta_box = array() ){

			$arr_amb = $this->_add_meta_box;

			// https://developer.wordpress.org/reference/functions/add_meta_box/
			// add_meta_box( string $id, string $title, callable $callback, string|array|WP_Screen $screen = null, string $context = 'advanced', string $priority = 'default', array $callback_args = null )
			$arr_callback = $arr_amb['callback'];
			if ( ! is_array($arr_amb['callback'])){
				$arr_callback = array($this, $arr_amb['callback']);
			}

			add_meta_box(
				$arr_amb['id'],
				$arr_amb['title'],
				$arr_callback,
				$arr_amb['screen'],
				$arr_amb['context'],
				$arr_amb['priority'],
				$arr_amb['callback_args']
			);
		}

		function add_meta_box_callback($post, $arr_callback_args = array() ){

			$this->ez_nonce( $this->_wp_nonce );
			echo $this->ez_loader_add_meta_boxes( $this->_wpezfields, $post, $this->_prefix, $this->_meta_box_id, '' );
			return;
		}



		public function ez_loader_add_meta_boxes( $arr_wpezfields, $post, $str_prefix = '', $str_meta_box_id = '', $str_wrapper = '' ) {

			$this->_prefix      = $str_prefix;
			$this->_field_class = $str_meta_box_id;
			$this->_crud        = 'add_meta_boxes';
			$this->_wrapper     = 'add_form_fields';
			if ( ! empty( $str_wrapper ) && method_exists( $this, $str_wrapper ) ) {
				$this->_wrapper = $str_wrapper;
			}
			$this->_post = $post;
			if ( ! empty( $this->ez_defaults()['add_meta_boxes_wrapper_class'] ) ) {
				return '<div class="' . esc_attr( $this->ez_defaults()['add_meta_boxes_wrapper_class'] ) . '">' . $this->ez_render( $arr_wpezfields ) . '</div>';
			} else {
				return $this->ez_render( $arr_wpezfields );
			}
		}


		/**
		 * @param $post_id
		 * @param $post
		 * @param string $update
		 *
		 * @return mixed
		 */
		function save_post_action($post_id, $post, $update = ''){

			// some additional save_post checks, aside from nonce
			if ( is_array($this->_post_types)){

				if ( ! in_array($post->post_type, $this->_post_types) ) {
					return $post_id;
				}

			} else{

				if ( $post->post_type != $this->_post_types ) {
					return $post_id;
				}
			}


			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
				return $post_id;
			}

			$post_types = get_post_type_object( $post->post_type );
			if ( ! current_user_can( $post_types->cap->edit_post, $post_id ) ) {
				return $post_id;
			}

			$this->ez_nonce( $this->_wp_nonce );
			$this->ez_loader_add_meta_boxes_save_post( $this->_wpezfields, $post_id, $post, $this->_prefix );
		}

		/**
		 * @param $arr_wpezfields
		 * @param $post_id
		 * @param $post
		 */
		public function ez_loader_add_meta_boxes_save_post( $arr_wpezfields, $post_id, $post ) {

			// if we have a nonce then we need to check it
			if ( $this->_nonce_active === true ) {

				if ( ! isset( $_POST[ $this->_nonce_name ] ) || ! wp_verify_nonce( $_POST[ $this->_nonce_name ], $this->_nonce_action ) ) {
					return;
				}
			}
			$this->ez_loader_save_fields( $arr_wpezfields, $post_id, 'get_post_meta', 'add_post_meta', 'update_post_meta', 'delete_post_meta' );
		}
	}
}