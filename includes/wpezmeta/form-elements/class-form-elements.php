<?php
/**
 * WPezFormEleents - Becoming a bit of a god class, but it works
 */
namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'Form_Elements' ) ) {
	class Form_Elements {

		protected $_register_meta_object_types = array();
		protected $_arr_defs_override = array();
		protected $_obj_sanitize = false;
		protected $_get_image_sizes = false;

		protected $_nonce_active = false;
		protected $_nonce_name;
		protected $_nonce_action;
		protected $_nonce_referer;
		protected $_nonce_echo;

		protected $_crud;
		protected $_post;
		protected $_term;
		protected $_prefix = '';
		protected $_field_class = '';
		protected $_wrapper;

		protected $_arr_teeny_mce_buttons = array();

		public function __construct() {

			add_action( 'admin_enqueue_scripts', array( $this, 'wp_enqueue_scripts_action' ), 20 );

		}

		/**
		 *
		 */
		public function wp_enqueue_scripts_action() {

			$str_folder = plugin_dir_url( __FILE__ );
			if ( $this->ez_defaults()['css_active'] !== false ) {

				wp_register_style( 'wpezfields-admin', $str_folder . 'assets/src/css/style.css', false, '0.0.7a' );
				wp_enqueue_style( 'wpezfields-admin' );
			}

			wp_enqueue_script( 'wpezfields-admin-file', $str_folder . 'assets/src/js/wp-file.js', array( 'jquery' ), '0.0.1' );

		}

		/**
		 * @param array $arr_defs_over
		 */
		public function ez_defaults_override( $arr_defs_over = array() ) {

			if ( is_array( $arr_defs_over ) ) {
				$this->_arr_defs_override = $arr_defs_over;
			}
		}

		/**
		 * @return array
		 */
		public function ez_defaults() {

			$arr_defs = array(
				'css_active'                   => true,
				'add_meta_boxes_wrapper_class' => 'wpezfields-add-meta-boxes-wrapper',
				'wrapper_class'                => 'wpezfields-field-wrapper',
				'desc_wrapper_tag'             => 'p',
				'desc_class'                   => 'wpezfields-desc',
			);

			if ( is_array( $this->_arr_defs_override ) ) {
				$arr_defs = array_merge( $arr_defs, $this->_arr_defs_override );
			}

			return $arr_defs;
		}


		/**
		 * pass in your own sanitize class / object
		 *
		 * @param bool $obj_sanitize
		 */
		public function set_sanitize( $obj_sanitize = false ) {

			if ( is_object( $obj_sanitize ) && method_exists( $obj_sanitize, 'sanitize' ) ) {
				$this->_obj_sanitize = $obj_sanitize;
			}
		}

		/**
		 * In the event that wpezfield values are (e.g.) derived from (e.g.) the post,
		 * 'init' and even 'add_meta_boxes' are both too soon. We have to wait. So this
		 * is a backdoor of sorts. Apparently the action 'posts_selection' is the earliest
		 * action to have $post available. This is rare but it's possible.
		 *
		 * We do however what to pass wpezfields in soon - sans the values - in order to
		 * do the register meta (which is tied to 'init')
		 *
		 * @param $arr_wpezfields
		 */
		public function set_wpezfields( $arr_wpezfields ) {
			if ( is_array( $arr_wpezfields ) ) {
				$this->_wpezfields = $arr_wpezfields;
			}
		}

		public function set_prefix( $str_prefix = '' ) {

			if ( ! empty( $str_prefix ) && is_string( $str_prefix ) ) {
				$this->_prefix = sanitize_key( $str_prefix );
			}
		}


		/**
		 * @param $str_meta_key
		 *
		 * @return string
		 */
		protected function prefix_meta_key( $str_meta_key ) {

			return sanitize_key( $this->_prefix . $str_meta_key );
		}

		/**
		 * @param $str_name
		 *
		 * @return mixed
		 */
		protected function prefix_name( $str_name ) {

			$str_temp = sanitize_file_name( $this->_prefix . $str_name );

			return str_replace( '-', '_', $str_temp );
		}


		/**
		 * @param array $arr_wpezfields
		 * @param string $str_prefix
		 */
		public function ez_register_meta( $arr_wpezfields = array(), $str_prefix = '' ) {

			foreach ( $arr_wpezfields as $key => $arr_field ) {
				if ( isset( $arr_field['active'] ) && $arr_field['active'] !== false ) {

					if ( isset( $arr_field['collection'] ) && is_array( $arr_field['collection'] ) ) {
						$this->ez_register_meta( $arr_field['collection'], $str_prefix );
					} else {

						if ( isset( $arr_field['register_meta'] ) && is_array( $arr_field['register_meta'] ) && isset( $arr_field['register_meta']['active'] ) && $arr_field['register_meta']['active'] !== false && isset( $arr_field['register_meta']['meta_key'] ) ) {

							if ( ! empty( $this->_register_meta_object_types ) && is_array( $this->_register_meta_object_types ) ) {
								foreach ( $this->_register_meta_object_types as $rmot ) {

									register_meta( $rmot, $this->prefix_meta_key( $arr_field['register_meta']['meta_key'] ), $arr_field['register_meta']['args'] );
								}
							} elseif ( isset( $arr_field['register_meta']['object_type'] ) ) {

								register_meta( $arr_field['register_meta']['object_type'], $this->prefix_meta_key( $arr_field['register_meta']['meta_key'] ), $arr_field['register_meta']['args'] );
							}
						}
					}
				}
			}
		}


		/**
		 * @param array $arr_nonce
		 */
		public function ez_nonce( $arr_nonce = array() ) {

			if ( is_array( $arr_nonce ) ) {
				$arr_nonce = array_merge( $this->ez_nonce_defaults(), $arr_nonce );
			} else {
				$arr_nonce = $this->ez_nonce_defaults();
			}

			$this->_nonce_active  = true;
			$this->_nonce_action  = $arr_nonce['action'];
			$this->_nonce_name    = $arr_nonce['name'];
			$this->_nonce_referer = $arr_nonce['referer'];
			$this->_nonce_echo    = $arr_nonce['echo'];
		}


		/**
		 * @return array
		 */
		protected function ez_nonce_defaults() {

			$arr_nonce_defs = array(
				'action'  => - 1,
				'name'    => '_wpnonce',
				'referer' => true,
				'echo'    => false,
				// note: WP's default is true, but we're doing it "The ezWay" and will echo it ourselves
			);

			return $arr_nonce_defs;
		}


		/**
		 * @param $arr_wpezfields
		 * @param string $wp_obj_id
		 * @param $str_wp_meta_get
		 * @param $str_wp_meta_add
		 * @param $str_wp_meta_update
		 * @param $str_wp_meta_delete
		 */
		protected function ez_loader_save_fields( $arr_wpezfields, $wp_obj_id = '', $str_wp_meta_get, $str_wp_meta_add, $str_wp_meta_update, $str_wp_meta_delete ) {

			// TODO - validation of args?
			foreach ( $arr_wpezfields as $key => $arr_field ) {

				// is the ez_field active?
				if ( isset( $arr_field['active'] ) && $arr_field['active'] !== false ) {

					// is the field a field or a sub_collection (i.e., predefined collection of fields)
					if ( isset( $arr_field['collection'] ) && is_array( $arr_field['collection'] ) ) {
						$this->ez_loader_save_fields( $arr_field['collection'], $wp_obj_id, $str_wp_meta_get, $str_wp_meta_add, $str_wp_meta_update, $str_wp_meta_delete );

					} else {
						if ( ! isset( $arr_field['name'] ) || ! isset( $arr_field['type'] ) || $arr_field['type'] == 'title' ) {
							continue;
						}
						/**
						 * TODO - custom field types will have to handle their own save
						 *
						 * if ( isset($arr_field['type']) && is_array($arr_field['type']) ){
						 *
						 * don't forget to pass the $str_wp_meta_get...etc
						 *
						 * continue;
						 * }
						 */
						$str_name = $this->prefix_name( $arr_field['name'] );
						// do we have a name and meta_key?
						if ( isset ( $_POST[ $str_name ] ) && isset( $arr_field['register_meta']['meta_key'] ) ) {

							// add the prefix to our meta_key
							$str_meta_key = $this->prefix_meta_key( $arr_field['register_meta']['meta_key'] );
							// sort out the sanitizing before we go any further
							$arr_field = $this->field_preprocessor_sanitize( $arr_field );

							/**
							 * TODO - Refactor - adding checkbox made this confused
							 */
							// checkbox is a special case since it's a "many"
							if ( $arr_field['type'] == 'checkbox' ) {
								if ( ! isset( $arr_field['register_meta']['args']['single'] ) || ( isset( $arr_field['register_meta']['args']['single'] ) && $arr_field['register_meta']['args']['single'] === true ) ) {

									$bool_continue = true;
									// single === true ? make the $_POST array into a string. we only need one meta row to store it.
									// TODO - sanitize? prior to serialize?
									$new = serialize( $_POST[ $str_name ] );
									$new = $this->ez_sanitize( $arr_field, $new );

								} elseif ( isset( $arr_field['options'] ) && is_array( $arr_field['options'] ) ) {
									// it's not a single - we're going to give each check its own meta row. good
									// for what you need to query this meta
									$bool_continue = false;
									$arr_posted    = array();
									if ( isset( $_POST[ $str_name ] ) && is_array( $_POST[ $str_name ] ) ) {
										$arr_posted = $_POST[ $str_name ];
									}

									// delete everything for this obj + meta_key
									$str_wp_meta_delete( $wp_obj_id, $str_meta_key );
									// refill
									foreach ( $arr_field['options'] as $val => $checked ) {
										if ( in_array( $val, $arr_posted ) ) {
											// sanitize it
											$new = $this->ez_sanitize( $arr_field, $val );
											// add it
											$str_wp_meta_add( $wp_obj_id, $str_meta_key, $new, false );
										}
									}
								}

							} else {


								$bool_continue = true;
								$new           = $this->ez_sanitize( $arr_field, $_POST[ $str_name ] );
							}
							if ( $bool_continue ) {
								$old = $str_wp_meta_get( $wp_obj_id, $str_meta_key, true );
								if ( $old && empty( $new ) ) {
									$str_wp_meta_delete( $wp_obj_id, $str_meta_key );
								} elseif ( $old !== $new ) {
									$str_wp_meta_update( $wp_obj_id, $str_meta_key, $new );
								}
							}


							/*
							 * ------------------------- input type = file ------------------------------
							 */
						} elseif ( isset ( $_FILES[ $str_name ] ) && ! empty( $_FILES[ $str_name ]['tmp_name'] ) && isset( $arr_field['register_meta']['meta_key'] ) ) {
							// https://github.com/tommcfarlin/WordPress-Upload-Meta-Box/blob/master/plugin.php

							// add the prefix to our meta_key
							$str_meta_key = $this->prefix_meta_key( $arr_field['register_meta']['meta_key'] );

							// TODO??? sort out the sanitizing before we go any further
							$arr_field = $this->field_preprocessor_sanitize( $arr_field );


							// Upload the goal image to the uploads directory, resize the image, then upload the resized version
							$get_remote = wp_remote_get( $_FILES[ $str_name ]['tmp_name'] );
							if ( is_wp_error( $get_remote ) ) {
								$get_remote = file_get_contents( $_FILES[ $str_name ]['tmp_name'] );
								// TODO check for error and try CURL
								// ref: https://tommcfarlin.com/wp_remote_get/
							}


							$goal_image_file = wp_upload_bits( $_FILES[ $str_name ]['name'], NULL, $get_remote );
							// Set post meta about this image. Need the comment ID and need the path.
							if ( $goal_image_file['error'] == false ) {

								// Since we've already added the key for this, we'll just update it with the file.
								update_post_meta( $wp_obj_id, $str_meta_key, $goal_image_file['url'] );

					// -----------

					// ??? https://codex.wordpress.org/Function_Reference/media_handle_upload

					// 	https://core.trac.wordpress.org/ticket/15311

					// !!! https://developer.wordpress.org/reference/functions/wp_get_image_editor/

					// >>>> https://metabox.io/meta-box/


								$upload_dir  = wp_upload_dir(); // Set upload folder
								$filename    = basename( $goal_image_file['url'] );
								$wp_filetype = wp_check_filetype( $filename, NULL );

								/**
								 * $arr_field['type_args']['ext_whitelist'] = array() of .exts
								 * -- ext in whitelist? include it!
								 */
								$bool_whitelist = true;
								if ( isset( $arr_field['type_args']['ext_whitelist'] ) && is_array( $arr_field['type_args']['ext_whitelist'] ) ) {
									if ( ! in_array( $wp_filetype['ext'], $arr_field['type_args']['ext_whitelist'] ) ) {
										$bool_whitelist = false;
										// continue
									}
								}
								/**
								 * $arr_field['type_args']['ext_blacklist'] = array() of .exts
								 * -- ext in blackist? eclude it!
								 */
								$bool_blacklist = true;
								if ( isset( $arr_field['type_args']['ext_blacklist'] ) && is_array( $arr_field['type_args']['ext_blacklist'] ) ) {
									if ( in_array( $wp_filetype['ext'], $arr_field['type_args']['ext_blacklist'] ) ) {
										$bool_blacklist = false;
										// continue
									}
								}
								// kinda silly to do it this way but for some reason it helps
								if ( $bool_whitelist == false || $bool_blacklist == false ) {
									// TODO - some sort of error msg?
									continue;
								}

								// Check folder permission and define file location
								if ( wp_mkdir_p( $upload_dir['path'] ) ) {
									$file = $upload_dir['path'] . '/' . $filename;
								} else {
									$file = $upload_dir['basedir'] . '/' . $filename;
								}

								// do we have an image?
								if ( preg_match( '!^image/!', $wp_filetype['type'] ) && file_is_displayable_image( $file ) ) {

					$arr_field['type_args']['image_sizes'] = array(
										'x' => array(
											'w'    => 500,
											'h'    => 600,
											'crop' => true,
											'set_quality' => 100,
											'flip' => array(
												'vert' => true
											),
										),
										'x1' => array(
											'w'    => 510,
											'h'    => 610,
											'crop' => true,
											'set_quality' => '10',
											'rotate' => 33
										),
										'y' => array(
											'w'    => 51,
											'h'    => 61,
											'crop' => true
										),
										'z' => array(
											'w'    => 52,
											'h'    => 62,
											'crop' => true
										)
									);
									/**
									 * $arr_field['type_args']['image_sizes']
									 * = false - no resizing
									 * = true - use WP registed image sizes
									 * = array() - of images sizes + crop bool
									 */
					$arr_field['type_args']['thumbnail'] = true;
									if ( isset( $arr_field['type_args']['image_sizes'] ) && $arr_field['type_args']['image_sizes'] !== false ) {

										// true === use the registered WP images sizes
										if ( $arr_field['type_args']['image_sizes'] === true ) {

											$arr_image_sizes = $this->get_image_sizes();

											// array === use custom sizes
										} elseif ( is_array( $arr_field['type_args']['image_sizes'] ) ) {

											$arr_image_sizes = $arr_field['type_args']['image_sizes'];
										}
										// take the image sizes and make some images
										$arr_sizes_meta = array();
										foreach ( $arr_image_sizes as $str_size => $arr_attrs ) {

											if ( isset( $arr_attrs['w'], $arr_attrs['h'], $arr_attrs['crop'] ) ) {
												$mix_imis = $this->image_make_intermediate_size( $file, intval( $arr_attrs['w'] ), intval( $arr_attrs['h'] ), (bool) $arr_attrs['crop'], $arr_attrs );
												if ( $mix_imis !== false ) {
													$arr_sizes_meta[ $str_size ] = $mix_imis;
												}
											}
										}
										/**
										 * $arr_field['type_args']['thumbnail']
										 * = true - make a thumbnail
										 * = false - don't make a thumbnail
										 */
										} elseif ( isset( $arr_field['type_args']['thumbnail'] ) && $arr_field['type_args']['thumbnail'] !== false ) {

										// fyi - 128 is WP default
										$w              = intval( get_option( 'thumbnail_size_w', 128 ) );
										// fyi - 96 is WP default
										$h              = intval( get_option( 'thumbnail_size_h', 96 ) );
										$crop           = (bool) get_option( 'thumbnail_copy', 1 );
										$arr_sizes_meta = array();
										$mix_imis       = $this->image_make_intermediate_size( $file, $w, $h, $crop, false);
										if ( $mix_imis !== false ) {
											$arr_sizes_meta['thumbnail'] = $mix_imis;
										}

									}
								}
								/**
								 * $arr_field['type_args']['media_library']
								 * - true = add the file to the media library.
								 */

								$arr_field['type_args']['media_library'] = true;
								if ( isset( $arr_field['type_args']['media_library'] ) && $arr_field['type_args']['media_library'] !== false ) {

									/*
									// Check folder permission and define file location
									if ( wp_mkdir_p( $upload_dir['path'] ) ) {
										$file = $upload_dir['path'] . '/' . $filename;
									} else {
										$file = $upload_dir['basedir'] . '/' . $filename;
									}
									*/
									$str_att_title = 'post_title';
									if ( isset( $arr_field['type_args']['attachment_title'] )) {
										$str_title = $arr_field['type_args']['attachment_title'];
									}
									global $post;
									switch ($str_att_title){

										case 'file_name':
											$str_title = 'TODO - file name';
											break;

										case 'post_title':
										default:
										$str_title = $post->post_title;

									}

										// Set attachment data
									$attachment = array(
										'file'           => $file,
										'post_mime_type' => $wp_filetype['type'],
										'post_title'     => esc_attr($str_title),
										'post_content'   => '',
										'post_status'    => 'inherit',
										'post_type'      => 'attachment'
									);

									$attach_id = wp_insert_post( $attachment, true );

									// -------------------

									// https://codex.wordpress.org/Function_Reference/wp_insert_attachment
									//		$attach_id = wp_insert_attachment( $attachment, $file, $wp_obj_id );

									// Include image.php
									require_once( ABSPATH . 'wp-admin/includes/image.php' );

									if ( ( isset( $arr_field['type_args']['image_sizes'] ) && is_array( $arr_field['type_args']['image_sizes'] ))
									|| ( isset($arr_field['type_args']['thumbnail']) && $arr_field['type_args']['thumbnail'] !== false )) {
										$arr_gis = getimagesize($file);
										$attach_data = array(
											'width'  => $arr_gis[0],
											'height' => $arr_gis[1],
											'file'   => $file,
											'sizes'  => $arr_sizes_meta,
										);
										// since we're creating our own sizes outside normal WP protocol,
										// we need to create this meta, which is used by the media gallery
										update_post_meta( $attach_id, '_wp_attachment_metadata', $attach_data );
									} else {

										// Define attachment metadata AND gen the images
										$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
										// Assign metadata to attachment
										wp_update_attachment_metadata( $attach_id, $attach_data );
									}
									// And finally assign featured image to post
									//	set_post_thumbnail( $wp_obj_id, $attach_id );
								}
							}
						} elseif ( isset( $arr_field['register_meta']['meta_key'] ) ) {
							$str_meta_key = $this->prefix_meta_key( $arr_field['register_meta']['meta_key'] );
							$str_wp_meta_delete( $wp_obj_id, $str_meta_key );
						}
					}
				}
			}
		}

		/**
		 * Resizes an image to make a thumbnail or intermediate size. "Forked" from wp-includes/media.php
		 *
		 * The returned array has the file size, the image width, and image height. The
		 * {@see 'image_make_intermediate_size'} filter can be used to hook in and change the
		 * values of the returned array. The only parameter is the resized file path.
		 *
		 * @since 2.5.0
		 *
		 * @param string $file   File path.
		 * @param int    $width  Image width.
		 * @param int    $height Image height.
		 * @param bool   $crop   Optional. Whether to crop image to specified width and height or resize.
		 *                       Default false.
		 *
		 * @return false|array False, if no image was created. Metadata array on success.
		 */
		protected function image_make_intermediate_size( $file, $width, $height, $crop = false, $arr_field_type_args = false ) {

			if ( $width || $height ) {
				$editor = wp_get_image_editor( $file );

				if ( is_wp_error( $editor ) ){
					return false;
				}

				$arr_ret = $this->image_make_intermediate_size_extended($editor, $arr_field_type_args);

				if ( is_wp_error( $editor->resize( $width, $height, $crop ) ) ){
					return false;
				}

				// TODO allow for *_extended to be applied pre and post resize()

				$resized_file = $editor->save();

				if ( ! is_wp_error( $resized_file ) && $resized_file ) {
					unset( $resized_file['path'] );
					return $resized_file;
				}
			}
			return false;
		}

		protected function image_make_intermediate_size_extended($editor, $arr_field_type_args = array()){

			$arr_ret = array();
			// Crops Image - crop( $src_x, $src_y, $src_w, $src_h, $dst_w = null, $dst_h = null, $src_abs = false )
			if (isset($arr_field_type_args['crop']) ){

				$arr_crop_args = $arr_field_type_args['crop'];

				if ( isset($arr_crop_args['src_x'], $arr_crop_args['src_y'], $arr_crop_args['src_w'], $arr_crop_args['src_h'] ) ){

					$dst_w = null;
					if ( isset($arr_crop_args['dst_w']) ){
						$dst_w = intval($arr_crop_args['dst_w']);
					}
					$dst_h = null;
					if ( isset($arr_crop_args['dst_h']) ){
						$dst_h = intval($arr_crop_args['dst_h']);
					}
					$src_abs = false;
					if ( isset($arr_crop_args['src_abs']) ){
						$src_abs = (boolean)$arr_crop_args['src_abs'];
					}

					$arr_ret['crop'] = $editor->crop( (int)$arr_crop_args['src_x'], (int)$arr_crop_args['src_y'], (int)$arr_crop_args['src_w'], (int)$arr_crop_args['src_h'], $dst_w, $dst_h, $src_abs );
				}
			}

			// rotate( $angle ) - Rotates current image counter-clockwise by $angle.
			if (isset($arr_field_type_args['rotate']) ){
				$arr_ret['rotate'] = $editor->rotate((integer)$arr_field_type_args['rotate']);
			}

			// flip( $horz, $vert ) - Flips current image on the horizontal or vertical axis.
			if ( isset($arr_field_type_args['flip']) ){

				$horz = false;
				if ( isset($arr_field_type_args['flip']['horz']) ){
					$horz = (boolean)$arr_field_type_args['flip']['horz'];

				}
				$vert = false;
				if ( isset($arr_field_type_args['flip']['vert']) ){
					$vert = (boolean)$arr_field_type_args['flip']['vert'];
				}
				$arr_ret['flip'] = $editor->flip($horz, $vert);
			}

			// Sets Image Compression quality on a 1-100% scale as an integer (1-100). Default quality defined in WP_Image_Editor class is 90.
			if (isset($arr_field_type_args['set_quality']) ){
				$arr_ret['set_quality'] = $editor->set_quality( (integer)$arr_field_type_args['set_quality'] );
			}
			return $arr_ret;
		}


		// ref: https://codex.wordpress.org/Function_Reference/get_intermediate_image_sizes
		// see examples
		protected function get_image_sizes() {

			global $_wp_additional_image_sizes;

			if ( $this->_get_image_sizes !== false ) {
				return $this->_get_image_sizes;
			}

			$sizes = array();

			foreach ( get_intermediate_image_sizes() as $_size ) {
				if ( in_array( $_size, array( 'thumbnail', 'medium', 'medium_large', 'large' ) ) ) {
					$sizes[ $_size ]['w']    = get_option( "{$_size}_size_w" );
					$sizes[ $_size ]['h']    = get_option( "{$_size}_size_h" );
					$sizes[ $_size ]['crop'] = (bool) get_option( "{$_size}_crop" );
				} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
					$sizes[ $_size ] = array(
						'w'    => $_wp_additional_image_sizes[ $_size ]['width'],
						'h'    => $_wp_additional_image_sizes[ $_size ]['height'],
						'crop' => $_wp_additional_image_sizes[ $_size ]['crop'],
					);
				}
			}
			$this->_get_image_sizes = $sizes;

			return $sizes;
		}


		/**
		 * A set of common sanitizers. This WILL evolve as necesary
		 * ref: https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data
		 *
		 * @param $arr_args
		 * @param $value
		 *
		 * @return string
		 */
		function ez_sanitize( $arr_field, $value ) {

			$str_sanitize = '';
			if ( isset( $arr_field['sanitize'] ) ) {
				$str_sanitize = $arr_field['sanitize'];
			}
			if ( $this->_obj_sanitize !== false ) {

				return $this->_obj_sanitize->sanitize( $str_sanitize, $value, $arr_field );

			} else {
				// no sanitize injected, then we'll have this fallback
				switch ( $str_sanitize ) {

					case 'sanitize_email':
						// https://codex.wordpress.org/Function_Reference/sanitize_email
						$value = sanitize_email( $value );
						break;

					case 'sanitize_file_name':
						// https://codex.wordpress.org/Function_Reference/sanitize_file_name
						$value = sanitize_file_name( $value );
						break;

					case 'sanitize_html_class':
						// https://codex.wordpress.org/Function_Reference/sanitize_html_class
						$value = sanitize_html_class( $value );
						break;

					case 'sanitize_key':
						// https://codex.wordpress.org/Function_Reference/sanitize_key
						$value = sanitize_key( $value );
						break;

					case 'sanitize_meta':
						// https://codex.wordpress.org/Function_Reference/sanitize_meta
						// TODO isset()?
						$value = sanitize_meta( $arr_field['register_meta']['meta_key'], $value, $arr_field['wp_type'] );
						break;

					case 'sanitize_mime_type':
						// https://codex.wordpress.org/Function_Reference/sanitize_mime_type
						$value = sanitize_mime_type( $value );
						break;


					case 'sanitize_option':
						// https://codex.wordpress.org/Function_Reference/sanitize_option
						$value = sanitize_key( $value );
						break;

					case 'sanitize_sql_orderby':
						// https://codex.wordpress.org/Function_Reference/sanitize_sql_orderby
						$value = sanitize_sql_orderby( $value );
						break;

					case 'sanitize_text_field':
						// https://developer.wordpress.org/reference/functions/sanitize_text_field/
						$value = sanitize_text_field( $value );
						break;

					case 'sanitize_title':
						// https://codex.wordpress.org/Function_Reference/sanitize_title
						$value = sanitize_title( $value );
						break;

					case 'sanitize_title_for_query':
						// https://codex.wordpress.org/Function_Reference/sanitize_title_for_query
						$value = sanitize_title_for_query( $value );
						break;

					case 'sanitize_title_with_dashes':
						// https://codex.wordpress.org/Function_Reference/sanitize_title_with_dashes
						$value = sanitize_title_with_dashes( $value );
						break;

					case 'sanitize_user':
						// https://codex.wordpress.org/Function_Reference/sanitize_user
						$value = sanitize_user( $value );
						break;

					case 'wp_kses_post':
						// https://codex.wordpress.org/wp_kses_post
						$value = wp_kses_post( $value );
						break;

					case 'sanitize_hex':
						// http://themehybrid.com/weblog/introduction-to-wordpress-term-meta
						$value = ltrim( $value, '#' );
						$value = preg_match( '/([A-Fa-f0-9]{3}){1,2}$/', $value ) ? $value : '';
						break;

					// wanna live dangerously? BE CAREFUL!
					case false:

						break;
					default:
						$value = sanitize_text_field( $value );
				}

				return $value;
			}
		}


		/**
		 * @param array $arr_wpezfields
		 *
		 * @return string
		 */
		public function ez_render( $arr_wpezfields = array() ) {

			$str_ret = '';
			if ( $this->_nonce_active === true ) {
				$str_ret .= wp_nonce_field(
					$this->_nonce_action,
					$this->_nonce_name,
					$this->_nonce_referer,
					$this->_nonce_echo
				);
			}
			$str_ret .= $this->ez_render_fields( $arr_wpezfields );

			return $str_ret;
		}

		/**
		 * @param array $arr_args
		 *
		 * @return string
		 */
		public function ez_render_fields( $arr_wpezfields = array() ) {

			$str_ret = '';

			foreach ( $arr_wpezfields as $key => $arr_field ) {

				if ( isset( $arr_field['active'] ) && $arr_field['active'] !== false ) {

					if ( isset( $arr_field['collection'] ) && is_array( $arr_field['collection'] ) ) {
						$str_ret .= $this->ez_render_fields( $arr_field['collection'] );
					} else {

						$arr_field        = $this->field_preprocessor( $arr_field );
						$str_label        = $this->label( $arr_field );
						$str_tooltip      = $this->tooltip( $arr_field );
						$str_form_element = $this->form_element( $arr_field );
						$str_desc         = $this->desc( $arr_field );

						//if ( hot or not? ) {
						$str_ret .= $this->wrapper( $arr_field, $str_label, $str_tooltip, $str_form_element, $str_desc );
						if ( isset($arr_field['hr']) && $arr_field['hr'] !== false ){
							$str_ret .= '<hr>';
						}
						// }
					}
				}
			}

			return $str_ret;
		}

		/**
		 * If some of a field's args aren't set (e.g., 'id' =>) we'll create them
		 *
		 * @param $arr_field
		 *
		 * @return mixed
		 */
		protected function field_preprocessor( $arr_field ) {

			// if there's a wp_editor then we need to see if there are custom buttons.
			// note: the field's ['type_args']['teeny'] must be set to true. else the filter that uses this array will be ignored.
			if ( isset( $arr_field['type'], $arr_field['name'], $arr_field['type_args']['teeny_mce_buttons'] )){

				if ( $arr_field['type'] == 'wp_editor' && is_array($arr_field['type_args']['teeny_mce_buttons']) && ! isset($this->_arr_teeny_mce_buttons[$this->prefix_name( $arr_field['name'])]) ){

					$this->_arr_teeny_mce_buttons[$this->prefix_name( $arr_field['name'])] = $arr_field['type_args']['teeny_mce_buttons'];
				}
			}

			if ( ! isset( $arr_field['wrapper_class'] ) ) {

				$arr_class = array();

				$str_temp    = $this->_prefix . 'wpezfields-field-wrapper';
				$arr_class[] = $this->html_class( $str_temp );

				$str_temp    = 'wpezfields-field-' . $arr_field['type'] . '-wrapper';
				$arr_class[] = $this->html_class( $str_temp );

				$str_temp    = $this->_prefix . 'wpezfields-field-' . $arr_field['type'] . '-wrapper';
				$arr_class[] = $this->html_class( $str_temp );

				$str_temp    = $this->_prefix . $this->_field_class . '-field_wrapper';
				$arr_class[] = $this->html_class( $str_temp );

				$str_temp    = $this->_prefix . $this->_field_class . '-' . $arr_field['type'] . '-wrapper';
				$arr_class[] = $this->html_class( $str_temp );

				$arr_field['wrapper_class'] = implode( ' ', $arr_class );
			}

			if ( ! isset( $arr_field['id'] ) ) {
				$arr_field['id'] = sanitize_html_class( $arr_field['name'] );
			}

			if ( ! isset( $arr_field['class'] ) ) {

				$arr_class = array();

				$str_temp    = $this->_prefix . $this->_field_class . '-field';
				$arr_class[] = $this->html_class( $str_temp );

				$str_temp    = $this->_prefix . $this->_field_class . '-' . $arr_field['type'];
				$arr_class[] = $this->html_class( $str_temp );

				$str_temp    = 'wpezfields-field-' . $arr_field['type'];
				$arr_class[] = $this->html_class( $str_temp );

				$arr_field['class'] = implode( ' ', $arr_class );
			}

			$arr_field = $this->field_preprocessor_sanitize( $arr_field );

			return $arr_field;
		}

		/**
		 * @param $str_class
		 *
		 * @return string
		 */
		protected function html_class( $str_class ) {

			return sanitize_html_class( str_replace( '_', '-', $str_class ) );
		}


		/**
		 * For a given type, we'll make presumptions about the sanitization
		 *
		 * @param $arr_field
		 *
		 * @return mixed
		 */
		protected function field_preprocessor_sanitize( $arr_field ) {

			if ( ! isset( $arr_field['sanitize'] ) ) {

				switch ( $arr_field['type'] ) {

					case 'checkbool':
					case 'checkbox':
					case 'color':
					case 'date':
					case 'datetime':
					case 'datetime-local':
					case 'month':
						// TODO - number
					case 'number':
					case 'password':
					case 'radio':
					case 'range':
					case 'search':
					case 'tel':
					case 'text':
					case 'time':
					case 'week':
						$str_sanitize = 'sanitize_text_field';
						break;

					case 'email':
						$str_sanitize = 'sanitize_email';
						break;

					case 'textarea':
						$str_sanitize = 'esc_textarea';
						break;

					case 'url':
						$str_sanitize = 'esc_url';
						break;
					case 'wp_editor':
						$str_sanitize = 'wp_kses_post';
						break;

					// not sure this is necessary but we'll add it - for now.
					case 'title':
						$str_sanitize = false;
						break;

					//TODO - other defaults
					default:
						$str_sanitize = 'sanitize_text_field';
				}
				$arr_field['sanitize'] = $str_sanitize;
			}

			return $arr_field;
		}

		/**
		 * @param $arr_field
		 * @param $str_label
		 * @param $str_tooltip
		 * @param $str_form_element
		 * @param $str_desc
		 *
		 * @return string
		 */
		public function wrapper( $arr_field, $str_label, $str_tooltip, $str_form_element, $str_desc ) {

			$str_wrap = trim( $this->_crud );
			if ( $str_wrap == 'add_meta_boxes' ) {
				$str_wrap = 'add_form_fields';
			}

			$str_method = 'wrapper_' . $this->_wrapper;

			if ( method_exists( $this, $str_method ) ) {

				return $this->$str_method( $arr_field, $str_label, $str_tooltip, $str_form_element, $str_desc );
			}

			return '';
		}


		/**
		 * Taxonomy: Add term
		 *
		 * @param array $arr_args
		 * @param string $str_label
		 * @param string $str_form_element
		 * @param string $str_desc
		 *
		 * @return string
		 */
		function wrapper_add_form_fields( $arr_field = array(), $str_label = '', $str_tooltip = '', $str_form_element = '', $str_desc = '' ) {

			$str_wrap = '';

			if ( ! empty( $this->ez_defaults()['wrapper_class'] ) ) {
				$str_wrap = ' ' . esc_attr( $this->ez_defaults()['wrapper_class'] ) . ' ';
			}

			$str_id = '';
			if ( isset( $arr_field['wrapper_id'] ) ) {
				$str_id = 'id="' . esc_attr( $arr_field['wrapper_id'] ) . '"';
			}

			$str_special = '';
			if ( isset( $arr_field['wrapper_class_special'] ) ) {
				$str_special = esc_attr( $arr_field['wrapper_class_special'] ) . ' ';
			}

			$str_ret = '';
			$str_ret .= '<div ' . $str_id . ' class="form-field ' . $str_special . $str_wrap . esc_attr( $arr_field['wrapper_class'] ) . '">';
			// TODO add returns

			$str_ret .= '<span class="wpezfields-label-tooltip-wrapper">';
			$str_ret .= $str_label;
			$str_ret .= $str_tooltip;
			$str_ret .= '</span>';
			$str_ret .= $str_form_element;
			$str_ret .= $str_desc;
			$str_ret .= '</div>';

			return $str_ret;
		}

		/**
		 * * Taxonomy: Edit term
		 *
		 * @param array $arr_args
		 * @param string $str_label
		 * @param string $str_form_element
		 * @param string $str_desc
		 *
		 * @return string
		 */
		function wrapper_edit_form_fields( $arr_field = array(), $str_label = '', $str_tooltip = '', $str_form_element = '', $str_desc = '' ) {

			$str_wrap = '';
			if ( ! empty( $this->ez_defaults()['wrapper_class'] ) ) {
				$str_wrap = ' ' . esc_attr( $this->ez_defaults()['wrapper_class'] ) . ' ';
			}

			$str_special = '';
			if ( isset( $arr_field['wrapper_class_special'] ) ) {
				$str_special = ' ' . esc_attr( $arr_field['wrapper_class_special'] ) . ' ';
			}

			$str_ret = '';
			$str_ret .= '<tr class="form-field' . $str_special . $str_wrap . esc_attr( $arr_field['wrapper_class'] ) . '">';
			$str_ret .= '<th scope="row">';
			$str_ret .= '<span class="wpezfields-label-tooltip-wrapper">';
			$str_ret .= $str_label;
			$str_ret .= $str_tooltip;
			$str_ret .= '</span>';
			$str_ret .= '</th>';
			$str_ret .= '<td>';
			$str_ret .= $str_form_element;
			$str_ret .= $str_desc;
			$str_ret .= '</td>';
			$str_ret .= '</tr>';

			return $str_ret;
		}

		/**
		 * @param array $arr_field
		 *
		 * @return string
		 */
		public function label( $arr_field = array() ) {

			$str_ret = '';
			if ( isset( $arr_field['id'] ) && isset( $arr_field['label'] ) ) {

				if ( $arr_field['type'] == 'radio' || $arr_field['type'] == 'checkbox' ) {
					$str_ret .= '<span class="wpezfields-not-screen-reader-text">';
					$str_ret .= esc_attr( $arr_field['label'] );
					// TODO > $str_ret .= 'required';
					$str_ret .= '</span>';

				} elseif ( $arr_field['type'] == 'title' ) {

					if ( isset($arr_field['type_args']['icon_class']) ){
						$str_ret .= '<span class="' . esc_attr($arr_field['type_args']['icon_class']). '">';
						$str_ret .= '</span> ';
					}

					$str_ret .= ' <span class="wpezfields-title">';
					$str_ret .= esc_attr( $arr_field['label'] );
					// TODO > $str_ret .= 'required';
					$str_ret .= '</span>';

				} else {
					$str_ret .= '<label for="' . esc_attr( $arr_field['id'] ) . '">';
					$str_ret .= esc_attr( $arr_field['label'] );
					// TODO > $str_ret .= 'required';
					$str_ret .= '</label>';
				}
			}

			return $str_ret;
		}

		/**
		 * Ref: https://chrisbracco.com/a-simple-css-tooltip/
		 *
		 * @param array $arr_args
		 *
		 * @return string
		 */
		public function tooltip( $arr_args = array() ) {

			$str_ret = '';
			if ( isset( $arr_args['tooltip'] ) && ! empty( esc_attr( $arr_args['tooltip'] ) ) ) {

				$str_ret .= '<span class="wpezfields-tooltip-wrapper">';
				$str_ret .= '<a href="#" data-tooltip="' . esc_attr( $arr_args['label'] ) . ' - ' . wp_kses_post( $arr_args['tooltip'] ) . '">';
				$str_ret .= '<span class="dashicons dashicons-editor-help"></span>';
				$str_ret .= '</a>';
				$str_ret .= '</span>';
			}

			return $str_ret;
		}

		/**
		 * @param array $arr_args
		 *
		 * @return string
		 */
		public function desc( $arr_args = array() ) {

			$str_ret = '';
			if ( isset( $arr_args['desc']) && ! empty( esc_attr( $arr_args['desc'] ) ) ) {

				$str_tag_open  = '';
				$str_tag_close = '';
				if ( ! empty( $this->ez_defaults()['desc_wrapper_tag'] ) ) {

					$str_tag_open = '<' . esc_attr( $this->ez_defaults()['desc_wrapper_tag'] ) . '>';
					if ( ! empty( $this->ez_defaults()['desc_class'] ) ) {
						$str_tag_open = '<' . esc_attr( $this->ez_defaults()['desc_wrapper_tag'] ) . ' class="' . esc_attr( $this->ez_defaults()['desc_class'] ) . ' ">';
					}
					$str_tag_close = '</' . esc_attr( $this->ez_defaults()['desc_wrapper_tag'] ) . '>';
				}

				$str_ret .= $str_tag_open;
				$str_ret .= esc_attr( $arr_args['desc'] );
				$str_ret .= $str_tag_close;
			}

			return $str_ret;
		}


		/**
		 * @param array $arr_args
		 *
		 * @return string
		 */
		public function form_element( $arr_args = array() ) {

			$str_ret = '';
			if ( isset( $arr_args['type'] ) && $arr_args['type'] != 'title' ) {

				$bool_single = true;
				if ( isset( $arr_args['register_meta']['args']['single'] ) ) {
					$bool_single = (boolean) $arr_args['register_meta']['args']['single'];
				}

				$value = '';
				switch ( $this->_crud ) {

					case 'add_form_fields':

						if ( isset( $arr_args['default_value'] ) ) {
							$value = $arr_args['default_value'];
						}
						break;

					case 'edit_form_fields':

						$term_id = $this->_term->term_id;
						$value   = get_metadata( 'term', $term_id, $this->prefix_meta_key( $arr_args['register_meta']['meta_key'] ), $bool_single );
						// if ( ( empty( $value ) || $value === false ) && isset( $arr_args['default_value'] ) ) {
						//	$value = $arr_args['default_value'];
						// }
						break;

					case 'add_meta_boxes':

						$post_id = $this->_post->ID;
						// if the post is new then use the default_value
						if ( substr( $this->_post->post_date_gmt, 0, 4 ) == '0000' && isset( $arr_args['default_value'] ) ) {
							$value = $arr_args['default_value'];
						} else {

							$value = get_metadata( 'post', $post_id, $this->prefix_meta_key( $arr_args['register_meta']['meta_key'] ), $bool_single );
						}
						//if ( ( empty( $value ) || $value === false ) && isset( $arr_args['default_value'] ) ) {
						//	$value = $arr_args['default_value'];
						// }
						break;
					default:

				}

				/**
				 * TODO - custom field types
				 *
				 * need to also address getting value (above) prior to the render
				 *
				 *
				 * if ( is_array($arr_args['type'])){
				 *
				 * if ( isset($arr_args['type'][0], $arr_args['type'][1]) ){
				 * $str_class = __NAMESPACE__ . '\\' . $arr_args['type'][0];
				 * $str_method = $arr_args['type'][1];
				 *
				 * if ( class_exists($str_class) ) {
				 * $new = new $str_class();
				 * if ( method_exists($new, $str_method )){
				 * return $new->$str_method($arr_args, $value, $this->_prefix);
				 * return 'method exists ' .  __NAMESPACE__;
				 * }
				 * return 'method ! exists';
				 * }
				 * return 'class ! exists ' . $str_class;
				 * }
				 * }
				 */


				switch ( $arr_args['type'] ) {

					case 'color':
					case 'date':
					case 'datetime':
					case 'datetime-local':
					case 'email':
					case 'file':
					case 'month':
					case 'number':
					case 'range':
					case 'search':
					case 'tel':
					case 'text':
					case 'time':
					case 'url':
					case 'week':

						$str_ret = $this->form_element_input( $arr_args, $value );
						break;

					case 'textarea':
						$str_ret = $this->form_element_textarea( $arr_args, $value );
						break;

					case 'frbool':
						$str_ret = $this->form_element_fmbool( $arr_args, $value );
						break;

					case 'img':
						$str_ret = $this->form_element_img( $arr_args, $value );
						break;

					case 'checkbool':
						$str_ret = $this->form_element_checkbool( $arr_args, $value );
						break;

					case 'select':
						$str_ret = $this->form_element_select( $arr_args, $value );
						break;

					case 'radio':
						$str_ret = $this->form_element_radio( $arr_args, $value );
						break;

					case 'checkbox':
						$str_ret = $this->form_element_checkbox( $arr_args, $value );
						break;

					case 'wp_editor':
						$str_ret = $this->form_element_wp_editor( $arr_args, $value );
						break;

					case 'title':
						$str_ret = '';
						break;

					default:
						$str_ret = '';
				}
			}

			return $str_ret;
		}

		/**
		 * @param array $arr_field
		 * @param string $value
		 *
		 * @return string
		 */
		public function form_element_checkbox( $arr_field = array(), $value = '' ) {

			$str_ret = '';

			if ( isset( $arr_field['options'] ) && is_array( $arr_field['options'] ) ) {

				$arr_value = array();
				if ( ! empty( $value ) && is_string( $value ) ) {
					$arr_value = unserialize( $value );
				} elseif ( ! empty( $value ) && is_array( $value ) ) {
					$arr_value = $value;
				}

				$str_ret .= '<fieldset id="' . esc_attr( $arr_field['id'] ) . '_fs" class="wpezfields-fieldset">';
				$str_ret .= '<legend class="screen-reader-text">' . esc_attr( $arr_field['label'] ) . '</legend>';

				//   <li><input type="checkbox" id="cb6" value="xcheese" /><label for="cb6>">Extra Cheese</label></li>
				foreach ( $arr_field['options'] as $val => $checked ) {

					$str_checked = '';
					// TODO - use default is no $value
					if ( in_array( $val, $arr_value ) ) {
						$str_checked = ' checked="checked" ';
					}

					$str_id = sanitize_key( $this->_prefix ) . esc_attr( $arr_field['name'] ) . '-' . sanitize_html_class( $val );
					$str_id = str_replace( '_', '-', strtolower( $str_id ) );

					$str_ret .= '<span class="wpezfields-checkbox-single-wrapper">';
					$str_ret .= '<input type="checkbox"';
					$str_ret .= ' value="' . esc_attr( $val ) . '"';
					$str_ret .= ' name="' . $this->prefix_name( $arr_field['name'] ) . '[]" ';
					$str_ret .= ' id="' . esc_attr( $str_id ) . '" ';
					$str_ret .= ' class="' . esc_attr( $arr_field['class'] ) . '" ';

					$str_ret .= $str_checked;
					if ( isset ( $arr_field['html_ir'] ) ) {
						$str_ret .= $this->input_restrictions( $arr_field );
					}
					$str_ret .= '>';
					$str_ret .= '<label for="' . esc_attr( $str_id ) . '">';
					$str_ret .= esc_attr( $checked );
					$str_ret .= '</label>';
					$str_ret .= '</span>';

				}
				$str_ret .= '</fieldset>';
			}

			return $str_ret;
		}


		/**
		 * @param array $arr_field
		 * @param string $value
		 *
		 * @return string
		 */
		public function form_element_radio( $arr_field = array(), $value = '' ) {

			$str_ret = '';

			if ( isset( $arr_field['options'] ) && is_array( $arr_field['options'] ) ) {

				$str_ret .= '<fieldset id="' . esc_attr( $arr_field['id'] ) . '_fs" class="wpezfields-fieldset">';
				$str_ret .= '<legend class="screen-reader-text">' . esc_attr( $arr_field['label'] ) . '</legend>';


				foreach ( $arr_field['options'] as $val => $choice ) {

					$str_checked = '';
					// TODO - use default is no $value
					if ( $val == $value ) {
						$str_checked = ' checked="checked" ';
					}

					$str_id = sanitize_key( $this->_prefix ) . esc_attr( $arr_field['name'] ) . '-' . sanitize_html_class( $val );
					$str_id = str_replace( '_', '-', strtolower( $str_id ) );

					$str_ret .= '<span class="wpezfields-radio-single-wrapper">';
					$str_ret .= '<input type="radio"';
					$str_ret .= ' value="' . esc_attr( $val ) . '"';
					$str_ret .= ' name="' . $this->prefix_name( $arr_field['name'] ) . '" ';

					$str_ret .= ' id="' . esc_attr( $str_id ) . '" ';
					$str_ret .= ' class="' . esc_attr( $arr_field['class'] ) . '" ';

					$str_ret .= $str_checked;
					if ( isset ( $arr_field['html_ir'] ) ) {
						$str_ret .= $this->input_restrictions( $arr_field );
					}
					$str_ret .= '>';
					$str_ret .= '<label for="' . esc_attr( $str_id ) . '">';
					$str_ret .= esc_attr( $choice );
					$str_ret .= '</label>';
					$str_ret .= '</span>';

				}
				$str_ret .= '</fieldset>';
			}

			return $str_ret;
		}

		/**
		 * @param array $arr_field
		 * @param string $value
		 *
		 * @return string
		 */
		public function form_element_input( $arr_field = array(), $value = '' ) {

			$str_ret = '';

			// TODO placeholder
			if ( isset( $arr_field['name'] ) ) {

				$arr_type_args = $this->input_defaults();
				if ( isset( $arr_field['type_args'] ) && is_array( $arr_field['type_args'] ) ) {
					$arr_type_args = array_merge( $this->input_defaults(), $arr_field['type_args'] );
				}

				$str_ret .= '<input type="' . esc_attr( $arr_field['type'] ) . '" ';
				$str_ret .= 'name="' . $this->prefix_name( $arr_field['name'] ) . '" ';
				if ( isset( $arr_field['id'] ) ) {
					$str_ret .= 'id="' . esc_attr( $arr_field['id'] ) . '" ';
				}
				if ( isset( $arr_field['class'] ) ) {
					$str_ret .= 'class="wpezfields-field ' . esc_attr( $arr_field['class'] ) . '"';
				}

				// if you set cols === false we'll ignore it.
				if ( $arr_type_args['placeholder'] !== false ) {
					$str_ret .= ' placeholder="' . esc_attr( $arr_type_args['placeholder'] ) . '" ';
				}

				if ( ! empty( $value ) ) {
					$str_ret .= ' value="' . esc_attr( $value ) . '" ';
				}

				if ( isset ( $arr_field['html_ir'] ) ) {
					$str_ret .= $this->input_restrictions( $arr_field );
				}
				$str_ret .= '>';
			}

			return $str_ret;
		}

		protected function input_defaults() {
			$arr_defs = array(
				'placeholder' => false
			);

			return $arr_defs;
		}


		/**
		 * TODO - not sure we care, browsers will ignore the unnecessary / unwanted
		 *
		 * @return array
		 */
		protected function html5_required() {

			$ret_arr = array(
				'text',
				'search',
				'url',
				'tel',
				'email',
				'password',
				'date pickers',
				'number',
				'checkbox',
				'radio',
				'file'
			);

			return $ret_arr;

		}

		/**
		 * Ref: https://www.w3schools.com/htmL/html_form_input_types.asp
		 *
		 * @param $arr_field
		 */
		protected function input_restrictions( $arr_field ) {

			$str_ret = '';
			if ( isset( $arr_field['html_ir'] ) && is_array( $arr_field['html_ir'] ) ) {

				$arr_html_ir = $arr_field['html_ir'];

				$arr_temp = array();
				if ( isset( $arr_html_ir['required'] ) ) {
					if ( $arr_html_ir['required'] === true ) {
						$arr_temp[] = 'required';
					}
					unset( $arr_html_ir['required'] );
				}

				if ( isset( $arr_html_ir['value'] ) ) {
					unset( $arr_html_ir['value'] );
				}

				foreach ( $arr_html_ir as $key => $val ) {

					$arr_temp[] = sanitize_key( $key ) . '="' . esc_attr( $val ) . '"';
				}
				$str_ret = implode( ' ', $arr_temp );

			}

			return $str_ret;
		}


		/**
		 * @param array $arr_field
		 * @param string $value
		 *
		 * @return string
		 */
		public function form_element_textarea( $arr_field = array(), $value = '' ) {

			$str_ret = '';

			// TODO placeholder
			if ( isset( $arr_field['name'] ) ) {

				$arr_type_args = $this->textarea_defaults();
				if ( isset( $arr_field['type_args'] ) && is_array( $arr_field['type_args'] ) ) {
					$arr_type_args = array_merge( $this->textarea_defaults(), $arr_field['type_args'] );

				}

				$str_ret .= '<textarea ';
				$str_ret .= 'name="' . $this->prefix_name( $arr_field['name'] ) . '"';
				if ( isset( $arr_field['id'] ) ) {
					$str_ret .= ' id="' . esc_attr( $arr_field['id'] ) . '"';
				}
				if ( isset( $arr_field['class'] ) ) {
					$str_ret .= ' class="wpezfields-field ' . esc_attr( $arr_field['class'] ) . '"';
				}

				// if you set cols === false we'll ignore it.
				if ( $arr_type_args['placeholder'] !== false ) {
					$str_ret .= ' placeholder="' . esc_attr( $arr_type_args['placeholder'] ) . '" ';
				}

				// if you set cols === false we'll ignore it.
				if ( $arr_type_args['cols'] !== false ) {
					$str_ret .= ' cols="' . esc_attr( $arr_type_args['cols'] ) . '" ';
				}
				// set rows === false and we'l ignore it.
				if ( $arr_type_args['rows'] !== false ) {
					$str_ret .= ' rows="' . esc_attr( $arr_type_args['rows'] ) . '" ';
				}
				if ( isset ( $arr_field['html_ir'] ) ) {
					$str_ret .= $this->input_restrictions( $arr_field );
				}
				$str_ret .= '>';
				$str_ret .= esc_textarea( $value );
				$str_ret .= '</textarea>';
			}

			return $str_ret;
		}


		protected function textarea_defaults() {

			$arr_defs = array(
				'cols'        => '40',
				'rows'        => '2',
				'placeholder' => false
			);

			// TODO add filter?
			return $arr_defs;
		}

		/**
		 * @param array $arr_args
		 * @param string $value
		 *
		 * @return string
		 */
		public function form_element_checkbool( $arr_args = array(), $value = '' ) {

			$str_ret = '';

			if ( isset( $arr_args['name'] ) ) {
				$str_ret .= '<input type="checkbox" ';
				$str_ret .= 'name="' . $this->prefix_name( $arr_args['name'] ) . '" ';
				if ( isset( $arr_args['id'] ) ) {
					$str_ret .= 'id="' . esc_attr( $arr_args['id'] ) . '" ';
				}
				if ( isset( $arr_args['class'] ) ) {
					$str_ret .= 'class="' . esc_attr( $arr_args['class'] ) . '" ';
				}
				if ( $value == 'on' ) {
					$str_ret .= ' checked="checked" ';
				}
				if ( isset ( $arr_field['html_ir'] ) ) {
					$str_ret .= $this->input_restrictions( $arr_field );
				}
				$str_ret .= '>';
			}

			return $str_ret;
		}

		/**
		 * @param array $arr_args
		 * @param string $value
		 *
		 * @return string
		 */
		public function form_element_select( $arr_args = array(), $value = '' ) {

			$str_ret = '';

			if ( isset( $arr_args['options'] ) && is_array( $arr_args['options'] ) ) {

				if ( isset( $arr_args['name'] ) ) {
					$str_ret .= '<select ';
					$str_ret .= 'name="' . $this->prefix_name( $arr_args['name'] ) . '" ';
					if ( isset( $arr_args['id'] ) ) {
						$str_ret .= 'id="' . esc_attr( $arr_args['id'] ) . '" ';
					}
					if ( isset( $arr_args['class'] ) ) {
						$str_ret .= 'class="' . esc_attr( $arr_args['class'] ) . '" ';
					}
					if ( isset ( $arr_field['html_ir'] ) ) {
						$str_ret .= $this->input_restrictions( $arr_field );
					}
					$str_ret .= '>';
					$str_opts = '';
					foreach ( $arr_args['options'] as $val => $choice ) {

						$str_checked = '';
						// TODO - use default is no $value
						if ( $val == $value ) {
							$str_checked = ' selected="selected"';
						}

						$str_opts .= '<option';
						$str_opts .= ' value="' . esc_attr( $val ) . '"';
						$str_opts .= $str_checked;
						$str_opts .= '>';
						$str_opts .= esc_attr( $choice );
						$str_opts .= '</option>';

					}
					$str_ret .= $str_opts;
					$str_ret .= '</select>';
				}
			}

			return $str_ret;
		}


		/**
		 * ref: https://codex.wordpress.org/Function_Reference/wp_editor
		 * ref: https://codex.wordpress.org/wp_kses_post
		 *
		 * @param array $arr_args
		 * @param string $value
		 *
		 * @return string
		 */
		public function form_element_wp_editor( $arr_field = array(), $value = '' ) {

			// TODO - evidently wp_editor / tiny mce is VERY picky about it's id.
			$str_id       = $this->prefix_name( $arr_field['name'] );
			$arr_type_args = array();
			// TODO type_args defaults for wp_editor
			if ( isset( $arr_field['type_args'] ) && is_array( $arr_field['type_args'] ) ) {
				$arr_type_args = $arr_field['type_args'];
			}

			// gotta ob_ this 'ish ;)
			ob_start();

			wp_editor( $value, $str_id, $arr_type_args );
			$ret_ob = ob_get_contents();

			ob_end_clean();

			return $ret_ob;
		}

		public function teeny_mce_buttons_filter($arr_buttons, $editor_id){

			if ( isset($this->_arr_teeny_mce_buttons[$editor_id]) && is_array($this->_arr_teeny_mce_buttons[$editor_id]) ){
				return $this->_arr_teeny_mce_buttons[$editor_id];
			}
			return $arr_buttons;
		}

	}
}
