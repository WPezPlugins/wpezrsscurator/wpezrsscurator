<?php
/**
 * TODO
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( !defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists('Add_Submenu_Page_Support') ){
	class Add_Submenu_Page_Support {


		public function __construct( $args = '' ) {

			add_action('admin_menu' , array($this, 'admin_menu_add_submenu_page') );


		}

		function admin_menu_add_submenu_page() {

			add_submenu_page( 'edit.php?post_type=wpezrsscurator', 'WPezRSSCurator: Support', 'Support', 'edit_posts', basename( __FILE__ ), array($this,'support') );
		}

		function support() {
			echo '<h1>WPezRSSCurator: Support</h1>';
			echo '<p>TODO</p>';
		}

	}
}
