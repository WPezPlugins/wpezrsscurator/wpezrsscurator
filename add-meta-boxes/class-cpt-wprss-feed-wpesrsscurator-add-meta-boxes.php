<?php
/**
 * Adds a meta box to WP RSS Agg's feed screen
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'CPT_WPRss_Feed_WPezRSSCurator_Add_Meta_Boxes' ) ) {
	class CPT_WPRss_Feed_WPezRSSCurator_Add_Meta_Boxes {

		protected $_post_types;

		//	protected $_nonce_name; //TODOx

		function __construct( $arr_args = array() ) {

			$this->_post_types = array( 'wprss_feed' );
			//	$this->_nonce_name = 'wpezrsscurator_wprss_feed_nonce';

			add_action( 'init', array( $this, 'init_wpezmeta_add_meta_boxes' ), 25 );
		}


		public function init_wpezmeta_add_meta_boxes() {

			$arr_args           = array(
				'active'       => true,
				'post_types'   => $this->_post_types,
				'add_meta_box' => $this->add_meta_box(),
				'prefix'       => '',
				'wpezfields'   => $this->wpezfields(),  // we pass an empty array because we'll set it later / below
				// optional
				/*'wp_nonce'     => array(
					'action' => basename( __FILE__ ),
					'name'   => $this->_nonce_name,
				)
				*/
			);
			$arr_priorities     = array();
			$new_Add_Meta_Boxes = new Add_Meta_Boxes( $arr_priorities );

			$new_Sanitize = new Sanitize();
			$new_Add_Meta_Boxes->set_sanitize( $new_Sanitize );
			$new_Add_Meta_Boxes->ez_loader( $arr_args );
		}

		/**
		 * the args for the add_meta_box
		 *
		 * @return array
		 */
		protected function add_meta_box() {

			$arr_amb = array(
				'id'       => 'cpt-wprss-feed-meta-box-wpezrsscurator',
				'title'    => __( 'WPezRSSCurator', 'wpezrss' ),
				//'callback' => 'add_meta_box_callback',
				'screen'   => $this->_post_types,
				'context'  => 'advanced',
				'priority' => 'high',
				//	'callback_args'
			);

			return $arr_amb;
		}

		protected function wpezfields() {

			$arr_wpezfields = array();

			$arr_wpezfields['wpezrss_feed_autopublish_section'] = array(
				'active'        => true,
				'name'          => 'wpezrss_feed_autopublish_section',
				'label'         => __( 'AutoPublish', 'wpesrss' ),
				'desc'          => __( 'For more auto-publish options get the AutoPublish add-on', 'wpezrss'),
				'tooltip'       => __( 'TODO', 'wpesrss' ),
				'type'          => 'title',
				'type_args'     => array(
					//	'icon_class' => 'dashicons dashicons-warning wpezfields-red'

				),
				'wrapper_class' => 'wpezfields-background-f7 wpezfields-border-top wpezfields-border-bottom'
			);

			$arr_wpezfields['wpezrss_auto_publish'] = array(
				'active'                => true,
				'name'                  => 'wpezrss_auto_publish',
				'label'                 => __( 'Auto-publish', 'wpezrss' ),
				'desc'                  => __( 'As WPezRSSCurator Items are added, their post status will automatically be set to published.', 'wpezrss' ),
				'tooltip'               => 'The randomizing is not exact. Your results may vary.',
				'type'                  => 'select',
				'options'               => array(
					'0' => __('Do not auto-publish. Leave all as draft', 'wpezrss' ),
					'10' => __('Auto-publish all new items', 'wpezrss' )
				),
				'wp_type'               => 'post',
				// https://codex.wordpress.org/Function_Reference/register_meta
				'register_meta'         => array(
					'active'   => true,
					'meta_key' => 'wpezrss_auto_publish',
					'args'     => array(
						// 'sanitize_callback' => 'sanitize_my_column_meta_key',
						// 'auth_callback' => 'authorize_my_column_meta_key',
						'type'         => 'string',
						'description'  => 'Desc TODO',
						'single'       => true,
						'show_in_rest' => false,
					)
				),
				'wrapper_class_special' => 'wpezfields-label-tooltip-wrapper-inline'
			);

			$arr_more = apply_filters( 'wpezrsscurator_wprss_feed_wpezrsscurator_add_meta_boxes', array() );
			if ( is_array( $arr_more ) ) {
				$arr_wpezfields = array_merge( $arr_wpezfields, $arr_more );
			}

			return $arr_wpezfields;
		}
	}
}