<?php
/**
 * The core's add_filter()
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'Core_Add_Filter' ) ) {
	class Core_Add_Filter {


		function __construct( $args = '' ) {

			add_filter( 'wp_feed_cache_transient_lifetime' , array($this, 'wp_feed_cache_transient_lifetime_filter') );


			// https://developer.wordpress.org/reference/functions/wp_title_rss/
			add_filter( 'wp_title_rss', array( $this, 'wp_title_rss_filter' ), 50, 1 );

			// https://developer.wordpress.org/reference/functions/self_link/
			add_filter( 'self_link', array( $this, 'self_link_filter' ), 20, 1 );

			// https://developer.wordpress.org/reference/functions/bloginfo_rss/
			add_filter( 'bloginfo_rss', array( $this, 'bloginfo_rss_filter_url' ), 20, 2 );
			add_filter( 'bloginfo_rss', array( $this, 'bloginfo_rss_filter_description' ), 20, 2 );
			add_filter( 'bloginfo_rss', array( $this, 'bloginfo_rss_filter_language' ), 20, 2 );

			// https://developer.wordpress.org/reference/hooks/rss_update_period/
			add_filter( 'rss_update_period', array( $this, 'rss_update_period_filter' ), 20, 1 );

			// https://developer.wordpress.org/reference/hooks/rss_update_frequency/
			add_filter( 'rss_update_frequency', array( $this, 'rss_update_frequency_filter' ), 20, 1 );

			/**
			 * ------------------ <item> ------------------
			 */

			/**
			 * https://developer.wordpress.org/reference/functions/get_post_comments_feed_link/
			 * TODO - available from original feed?
			 * for now we'' just use
			 * https://developer.wordpress.org/reference/functions/comments_open/
			 * to turn it off (i.e., return false
			 */
			add_filter( 'comments_open', array( $this, 'comments_open_filter' ), 20, 2 );

			// https://developer.wordpress.org/reference/functions/comments_link_feed/
			// TODO - can we get this from the incoming feed? add as post_meta? (Not easily?)
			add_filter( 'comments_link_feed', array( $this, 'comments_link_feed_filter' ), 20, 1 );

			// https://developer.wordpress.org/reference/functions/get_post_time/
			add_filter( 'get_post_time', array( $this, 'get_post_time_filter' ), 20, 3 );

			/**
			 * the_category_rss('rss2')
			 * https://developer.wordpress.org/reference/functions/get_the_category_rss/
			 *
			 * note: there's a filter at the end but ya might wanna filter
			 * get_the_category(); and get_the_tags()
			 */

			// https://developer.wordpress.org/reference/functions/the_guid/
			add_filter( 'the_guid', array( $this, 'the_guid_filter' ), 20, 2 );

			/**
			 * TODO?
			 *
			 * https://developer.wordpress.org/reference/functions/the_excerpt_rss/
			 * add_filter( 'the_excerpt_rss', array( $this, 'the_excerpt_rss_filter' ), 20, 2 );
			 */
		}


		protected function defaults() {

			$arr_defs = array(

				'wp_feed_cache_transient_lifetime_active' => true,
				'wp_feed_cache_transient_lifetime_seconds' => 1,

				// wp_title_rss_filter
				'wp_title_rss_active'             => true,
				'wp_title_rss'                    => __( 'It\'s a Secret', 'wpezrss' ),

				// self_link_filter
				'self_link_active'                => false,
				'self_link'                       => 'http://faux-url-1.com',

				// bloginfo_rss_filter_url
				'bloginfo_rss_url_active'         => false,
				'bloginfo_rss_url'                => 'http://faux-url-2.com',

				// bloginfo_rss_filter_description
				'bloginfo_rss_description_active' => true,
				'bloginfo_rss_description'        => __( 'Description', 'wpezrss' ),

				// bloginfo_rss_filter_language
				'bloginfo_rss_language_active'    => false,
				'bloginfo_rss_language'           => 'false',

				// rss_update_period_filter
				'rss_update_period_active'        => true,
				'rss_update_period'               => 'hourly',

				// rss_update_frequency_filter
				'rss_update_frequency_active'     => true,
				'rss_update_frequency'            => 1,

				// comments_open_filter
				'comments_open_active'            => true,
				'comments_open_bool'              => false,

				// TODO - get from original free? Posible but not obvious
				// comments_link_feed_filter
				'comments_link_feed_active'       => true,
				'comments_link_feed'              => 'http://faux-url-3.com',

				//  get_post_time_filter
				'get_post_time_active'            => true,
				'get_post_time'                   => '',

				// from the original post?
				// the_guid_filter
				'the_guid_active'                 => true,
				'the_guid'                        => 'http://faux-url-4.com/',
			);

			$arr_defs_override = apply_filters( 'wpezrsscurator_core_defaults_override', array() );
			if ( is_array( $arr_defs_override ) ) {
				return array_merge( $arr_defs, $arr_defs_override );
			}

			return $arr_defs;
		}

		/**
		 * Setting a new cache time for feeds in WordPress
		 */
		function wp_feed_cache_transient_lifetime_filter( $seconds ) {

			if ( $this->defaults()['wp_feed_cache_transient_lifetime_active'] !== false ) {

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();

				if ( $arr_term_meta === true ) {
				//	return $seconds;
				}

				return (integer) $this->defaults()['wp_feed_cache_transient_lifetime_seconds'];
			}
		}


		function wp_title_rss_filter( $feed_title ) {

			if ( $this->defaults()['wp_title_rss_active'] !== false ) {

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();

				if ( $arr_term_meta === true ) {
					return $feed_title;
				}

				return $this->defaults()['wp_title_rss'];
			}
		}

		function self_link_filter( $url ) {

			if ( $this->defaults()['self_link_active'] !== false ) {

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();

				if ( $arr_term_meta === true ) {
					return $url;
				}

				return esc_url( $this->defaults()['self_link'] );
			}
			return str_replace('&', '&amp;', $url);
		}

		function bloginfo_rss_filter_url( $get_bloginfo_rss = '', $show = '' ) {

			if ( $this->defaults()['bloginfo_rss_url_active'] !== false ) {

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();

				if ( $arr_term_meta === true ) {
					return $get_bloginfo_rss;
				}
				if ( $show == 'url' ) {
					return esc_url( $this->defaults()['bloginfo_rss_url'] );
				}
			}

			return $get_bloginfo_rss;
		}


		function bloginfo_rss_filter_description( $get_bloginfo_rss = '', $show = '' ) {

			if ( $this->defaults()['bloginfo_rss_description_active'] !== false ) {

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();

				if ( $arr_term_meta === true ) {
					return $get_bloginfo_rss;
				}
				if ( $show == 'description' ) {
					return esc_attr( $this->defaults()['bloginfo_rss_description'] );
				}
			}

			return $get_bloginfo_rss;
		}

		function bloginfo_rss_filter_language( $get_bloginfo_rss = '', $show = '' ) {

			if ( $this->defaults()['bloginfo_rss_language_active'] !== false ) {

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();

				if ( $arr_term_meta === true ) {
					return $get_bloginfo_rss;
				}
				if ( $show == 'language' ) {
					return esc_attr( $this->defaults()['bloginfo_rss_language'] );
				}
			}

			return $get_bloginfo_rss;
		}

		function rss_update_period_filter( $duration ) {

			if ( $this->defaults()['rss_update_period_active'] !== false ) {

				$arr_values = array( 'hourly', 'daily', 'weekly', 'monthly', 'yearly' );

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();

				if ( $arr_term_meta === true ) {
					return $duration;
				}
				if ( in_array( $this->defaults()['rss_update_period'], $arr_values ) ) {
					return $this->defaults()['rss_update_period'];
				}
			}

			return $duration;
		}


		function rss_update_frequency_filter( $frequency ) {

			if ( $this->defaults()['rss_update_frequency_active'] !== false ) {

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();

				if ( $arr_term_meta === true ) {
					return $frequency;
				}

				return (integer) $this->defaults()['rss_update_frequency'];
			}

			return $frequency;
		}


		function comments_open_filter( $open = true, $post_id = '' ) {

			if ( $this->defaults()['comments_open_active'] !== false ) {

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();

				if ( $arr_term_meta === true ) {
					return $open;
				}

				return (bool ) $this->defaults()['comments_open_bool'];
			}

			return $open;
		}


		function comments_link_feed_filter( $get_comments_link ) {

			if ( $this->defaults()['comments_link_feed_active'] !== false ) {

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();

				if ( $arr_term_meta === true ) {
					return $get_comments_link;
				}

				return esc_url( $this->defaults()['comments_link_feed'] );
			}

			return $get_comments_link;
		}

		/**
		 * TODO
		 *
		 * @param $time
		 * @param $d
		 * @param $gmt
		 *
		 * @return mixed
		 */
		function get_post_time_filter( $time, $d, $gmt ) {

			if ( $this->defaults()['get_post_time_active'] !== false ) {

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();
				if ( $arr_term_meta === true ) {
					return $time;
				}

				global $post;
				$orig_pub = get_post_meta($post->ID, 'wpezrss_item_publish_date', true);
				if ( $orig_pub !== false ){
					return $orig_pub;
				}
			}
			return $time;
		}


		/**
		 * https://developer.wordpress.org/reference/functions/the_guid/
		 */
		function the_guid_filter( $guid, $id = '' ) {

			if ( $this->defaults()['the_guid_active'] !== false ) {

				$new_Init_Request = new Init_Request();
				$arr_term_meta    = $new_Init_Request->get_term_meta();

				if ( $arr_term_meta === true ) {
					return $guid;
				}
				global $post;

				return $this->defaults()['the_guid'] . '?p=' . $post->ID;
			}

			return $guid;
		}

	}
}