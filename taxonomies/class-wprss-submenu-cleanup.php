<?php
/**
 * We add some of our taxs to the WP RSS Aggregate wprss_feed post type. But we don't wanna
 * list those in that menu. It's messy and unnecessary. This cleans up that mess.
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'WPRss_Submenu_Cleanup' ) ) {
	class WPRss_Submenu_Cleanup {


		function __construct() {

			add_action( 'admin_menu', array($this, 'admin_menu_wprss_submenu_cleanup'), 999 );

		}

		// http://stackoverflow.com/questions/7610702/wordpress-remove-submenu-from-custom-post-type/
		// href="edit.php?post_type=wprss_feed"
		// edit-tags.php?taxonomy=feed_requested&post_type=wprss_feed
		function admin_menu_wprss_submenu_cleanup() {

			global $submenu;
			$arr_submenu = $submenu['edit.php?post_type=wprss_feed'];
			if ( is_array($arr_submenu) ) {
				foreach ( $arr_submenu as $key => $arr ) {
					if ( $arr[1] == 'manage_categories' ) {
						unset( $submenu['edit.php?post_type=wprss_feed'][ $key ] );
					}
				}
			}
		}
	}
}
