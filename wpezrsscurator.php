<?php

/**
 * Plugin Name: WPezRSSCurator
 * Plugin URI: https://gitlab.com/wpez/wpezplugins/wpezrsscurator/wpezrsscurator
 * Description: This is an (unofficial) extension to WP RSS Aggregator (https://www.wprssaggregator.com/#utm_source=wpez&utm_medium=plugin&utm_campaign=wpezrss-curator)
 * Version: 0.5.0
 * Author: Mark "Chief Alchemist" Simchock for Alchemy United
 * Author URI: http://AlchemyUnited.com
 * Text Domain: wpezrss
 * Domain Path: /languages/
 * License: GPLv3
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( !defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists('Plugin_Core') ){
	class Plugin_Core {

		public function __construct( $args = '' ) {

			require_once 'includes/wpezmeta/form-elements/class-form-elements.php';

			require_once 'includes/wpezmeta/sanitize/class-sanitize.php';

			require_once 'includes/wpezmeta/add-meta-boxes/class-add-meta-boxes.php';

			require_once 'includes/wpezmeta/taxonomy-term-meta/class-taxonomy-term-meta.php';


			require_once 'init-request/class-init-request.php';
			$new_Init_Request = new Init_Request();

			// our CPT wpezrsscurator
			require_once 'cpt/class-cpt-wpezrsscurator.php';
			$new_CPT_WPezRSSCurator = new CPT_WPezRSSCurator();

			// a meta box for our CPT - uses WPezFields
			require_once 'add-meta-boxes/class-cpt-wpezrsscurator-add-meta-boxes.php';
			$new_CPT_WPezRSSCurator_Add_Meta_Boxes = new CPT_WPezRSSCurator_Add_Meta_Boxes();

			// the crux of the matter.
			require_once 'taxonomies/class-taxonomies.php';
			$new_Taxonomies      = new Taxonomies();

			require_once 'taxonomies/class-tax-feed-add-meta-column.php';
			$new_Tax_Feed_Add_Meta_Column = new Tax_Feed_Add_Meta_Column();

			// keep things clean - remove tax submenus from the wprss feed cpt. we don't really need them there
			require_once 'taxonomies/class-wprss-submenu-cleanup.php';
			$new_WPRSS_Submenu_Cleanup = new WPRss_Submenu_Cleanup();

			require_once 'add-meta-boxes/class-cpt-wprss-feed-wpesrsscurator-add-meta-boxes.php';
			$new_CPT_WPRss_Feed_WPezRSSCurator_Add_Meta_Boxes = new CPT_WPRss_Feed_WPezRSSCurator_Add_Meta_Boxes();

			// TODO
			require_once 'submenus/class-add-submenu-page-settings.php';
			$new_Add_Submenu_Page_Settings = new Add_Submenu_Page_Settings();

			// TODO
			require_once 'submenus/class-add-submenu-page-faq.php';
			$new_Add_Submenu_Page_FAQ = new Add_Submenu_Page_FAQ();

			// TODO
			require_once 'submenus/class-add-submenu-page-support.php';
			$new_Add_Submenu_Page_Support = new Add_Submenu_Page_Support();

			// hooks from wp rss agg that we need to manipulate
			require_once 'wprss/class-wprss-hooks.php';
			$new_WPRss_Hooks = new WPRss_Hooks();

			//
			require_once 'add-filter/class-core-add-filter.php';
			$new_Core_Add_Filter = new Core_Add_Filter();

		}

	}
	$obj_wpezrss_plugin = new Plugin_Core();
}