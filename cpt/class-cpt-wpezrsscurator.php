<?php
/**
 * The magic for the wpezrsscurator CPT happens here
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'CPT_WPezRSSCurator' ) ) {
	class CPT_WPezRSSCurator {

		protected $_singular;
		protected $_plural;

		protected $_arr_labels_override;
		protected $_arr_args_override;

		function __construct( $args = '' ) {

			$this->_singular = esc_attr( apply_filters( 'wpezrsscurator_cpt_wpezrsscurator_label_singular', __('WPezRSSCurator Item', 'wpezrss') ) );
			$this->_plural = esc_attr( apply_filters( 'wpezrsscurator_cpt_wpezrsscurator_label_plural', __('WPezRSSCurator Items', 'wpezrss') ) );

			$this->_arr_labels_override = apply_filters( 'wpezrsscurator_cpt_wpezrsscurator_labels_override', array() );
			$this->_arr_args_override = apply_filters( 'wpezrsscurator_cpt_wpezrsscurator_args_override', array() );

			add_action( 'init', array($this, 'init_register_post_type'), 20 );
		}


		function init_register_post_type() {

			// bring it all together
			$arr_args = $this->args();

			// make the post type
			register_post_type( 'wpezrsscurator', $arr_args );
		}


		function args(){

			$arr_labels = $this->default_labels();
			if ( is_array($this->_arr_labels_override) ){
				$arr_labels = array_merge($arr_labels,$this->_arr_labels_override );
			}
			$arr_args = $this->default_args();
			if ( is_array($this->_arr_args_override) ){
				$arr_args = array_merge($arr_args, $this->_arr_args_override );
			}

			if ( ! isset($arr_args['labels']) || ( isset($arr_args['labels']) && ! is_array($arr_args['labels'])) ){
				$arr_args['labels'] = $arr_labels;
			}
			return $arr_args;
		}

		function default_labels() {

			$singular = $this->_singular;
			$plural = $this->_plural;

			$arr_labels = array(
				'name'                  => _x( $plural, 'Post Type General Name', 'wpezrss' ),
				'singular_name'         => _x( $singular, 'Post Type Singular Name', 'wpezrss' ),
				'menu_name'             => __( $plural, 'wpezrss' ),
				'name_admin_bar'        => __( $plural, 'wpezrss' ),
				'archives'              => __( $singular, 'wpezrss' ) . ' Archives',
				'attributes'            => __( $singular , 'wpezrss' ) . ' Attributes',
				'parent_item_colon'     => __( $singular, 'wpezrss' )  . ':',
				'all_items'             => __( 'All ', 'wpezrss' ) . $plural,
				'add_new_item'          => 'Add New ' . __( $singular, 'wpezrss' ),
				'add_new'               => __( 'Add New', 'wpezrss' ),
				'new_item'              => __( 'New ', 'wpezrss' ) . $singular,
				'edit_item'             => __( 'Edit ', 'wpezrss' ) . $singular,
				'update_item'           => __( 'Update ', 'wpezrss' ) . $singular,
				'view_item'             => __( 'View ', 'wpezrss' ) . $singular,
				'view_items'            => __( 'View ', 'wpezrss' ) . $plural,
				'search_items'          => __( 'Search ', 'wpezrss' ) . $plural,
				'not_found'             => __( 'Sorry. Search found no ', 'wpezrss' ) . $plural,
				'not_found_in_trash'    => __( 'Not found in Trash', 'wpezrss' ),
				'featured_image'        => __( 'Featured Image', 'wpezrss' ),
				'set_featured_image'    => __( 'Set featured image', 'wpezrss' ),
				'remove_featured_image' => __( 'Remove featured image', 'wpezrss' ),
				'use_featured_image'    => __( 'Use as featured image', 'wpezrss' ),
				'insert_into_item'      => __( 'Insert into this ', 'wpezrss' ) . $singular,
				'uploaded_to_this_item' => __( 'Uploaded to this ', 'wpezrss' ) . $singular,
				'items_list'            => $plural . __( ' list', 'wpezrss' ),
				'items_list_navigation' => $plural . __( ' list navigation', 'wpezrss' ),
				'filter_items_list'     => $plural . __( ': Filter list', 'wpezrss' ),
			);

			return $arr_labels;
		}


		function default_args() {

			$args = array(
				'label'               => __( $this->_plural, 'wpezrss' ),
				'description'         => __( 'A staging area for curation and ultimately wpezrss_feed feeds.', 'wpezrss' ),
				'labels'              => 'wpezrss',
				// TODO - filter supports || just remove custom-fields
				'supports'            => array(
					'title',
					'editor',
					'excerpt',
					'author',
					'thumbnail',
					'comments',
					'revisions',
				//	'custom-fields',
				),
				'taxonomies'          => array(),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'menu_position'       => 5,
				'show_in_admin_bar'   => true,
				'show_in_nav_menus'   => true,
				'can_export'          => true,
				'has_archive'         => true,
				'menu_icon'           => 'dashicons-rss',
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'capability_type'     => 'page',
			);

			return $args;
		}
	}
}
