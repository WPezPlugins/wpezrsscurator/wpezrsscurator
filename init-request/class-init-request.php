<?php
/**
 * we get all the feed term meta once and only once, to be share across all the plugin add-ons
 */

namespace WPezPlugins\WPezRSSCurator;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'Init_Request' ) ) {
	class Init_Request {

		protected static $_term_meta = true;

		function __construct( $args = '' ) {

			add_filter( 'request', array( $this, 'request_filter' ), 5, 1 );
		}

		public function get_term_meta(){

			return self::$_term_meta;
		}

		public function request_filter( $arr_qvar ){

			if ( isset( $arr_qvar ['feed'])  ){

				if ( isset($arr_qvar ['wpezrss_feed']) && self::$_term_meta === true ){

					// get the term
					$term = sanitize_text_field($arr_qvar['wpezrss_feed']);
					// look up the term, we need the term_id
					$obj_term_meta = get_term_by('slug', $term, 'wpezrss_feed');
					// did we find something?
					if ( $obj_term_meta instanceof \WP_term ){

						// all the term meta in a single grab!
						self::$_term_meta = get_term_meta($obj_term_meta->term_id, '', true);
					} else {

						self::$_term_meta = false;
					}
				}
			}
			return  $arr_qvar;
		}
	}
}
