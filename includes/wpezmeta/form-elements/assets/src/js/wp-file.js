(function($) {
    "use strict";

    $(function() {
        // to get WP to allow file uploads
        $('form').attr('enctype', 'multipart/form-data');
    });
}(jQuery));